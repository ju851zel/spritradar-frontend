import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class SimpleTimeSeriesChart extends StatelessWidget {
  final List<ChartData> seriesList;
  final bool animate;

  SimpleTimeSeriesChart(this.seriesList, this.animate);

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      [
        charts.Series<ChartData, DateTime>(
          id: 'Prices',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (ChartData sales, _) => sales.time,
          measureFn: (ChartData sales, _) => sales.formattedPriceCents,
          data: seriesList,
        ),
      ],
      animate: animate,
      primaryMeasureAxis: new charts.NumericAxisSpec(
          tickProviderSpec:
              new charts.BasicNumericTickProviderSpec(zeroBound: false)),
      // Optionally pass in a [DateTimeFactory] used by the chart. The factory
      // should create the same type of [DateTime] as the data provided. If none
      // specified, the default creates local date time.
      dateTimeFactory: const charts.LocalDateTimeFactory(),
    );
  }
}

class ChartData {
  final String id;
  final DateTime time;
  final int formattedPriceCents;

  ChartData(this.id, this.time, this.formattedPriceCents);
}
