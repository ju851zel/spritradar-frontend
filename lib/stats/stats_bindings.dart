import 'package:get/get.dart';
import 'package:spritradar/services/rest_service.dart';
import 'package:spritradar/stats/stats_controller.dart';
import 'package:spritradar/stats/stats_page.dart';


class StatsBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<StatsControllerInterface>(StatsController());
  }
}
