import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:spritradar/stats/stats_charts.dart';
import 'package:spritradar/stats/stats_model.dart';
import 'package:spritradar/stats/stats_page.dart';
import 'package:fluttertoast/fluttertoast.dart';

class StatsController extends GetxController
    implements StatsControllerInterface {
  final StatsRestService service = Get.find();
  final TextEditingController _endController = TextEditingController();
  final TextEditingController _starController = TextEditingController();
  final TextEditingController _stationController = TextEditingController();
  RxList<List<ChartData>> _stats = <List<ChartData>>[].obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    fetchStats();
    _starController.text = "2022-01-07T20:00:00Z";
    _endController.text = "2022-01-08T20:00:00Z";
    _stationController.text = "38D34D53-F3BE-44EC-9FBF-EB1D95BC2ACE";
  }

  void fetchStats() async {
    final List<List<ChartData>> data = (await service.fetchStats())
        .map((e) => e.petrolPrice
            .map((price) => ChartData(
                e.id,
                DateTime.parse(price.timestampIsoString),
                price.superE5DeciCents))
            .toList())
        .toList();
    data.map((e) {
      var prices = e;
      prices.sort((a, b) => a.time.isAfter(b.time) ? 0 : 1);
      return prices;
    });
    _stats.value = data;
  }

  @override
  TextEditingController get endController => _endController;

  @override
  RxList<List<ChartData>> get stats => _stats;

  @override
  Future<void> runQuery() async {
    String? stationId = null;
    if (_stationController.text.isNotEmpty) {
      stationId = _stationController.text;
    }
    var response = await service.sendStatsRequest(
      stationId: stationId,
      startTs: _starController.text,
      endTs: _endController.text,
    );
    if (response == 200) {
      Fluttertoast.showToast(
          msg:
              "Send query, coma back later to watch you stats. This can take a while.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    Future.delayed(Duration(seconds: 1), () => fetchStats());
    Future.delayed(Duration(seconds: 2), () => fetchStats());
    Future.delayed(Duration(seconds: 4), () => fetchStats());
    Future.delayed(Duration(seconds: 8), () => fetchStats());
  }

  @override
  TextEditingController get startController => _starController;

  @override
  TextEditingController get stationController => _stationController;

  @override
  void deleteStat({required String statId}) {
    service.deleteStat(statId: statId);
    Future.delayed(Duration(seconds: 1), () {
      fetchStats();
    });
  }

  @override
  void fetchQuery() {
    fetchStats();
  }

  @override
  bool get isLoggenIn => service.isLoggedIn1();
}

abstract class StatsRestService {
  Future<int> sendStatsRequest({
    required String? stationId,
    required String startTs,
    required String endTs,
  });

  Future<List<Stats>> fetchStats();

  void deleteStat({required String statId});

  bool isLoggedIn1();
}
