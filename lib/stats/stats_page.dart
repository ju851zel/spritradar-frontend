import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:spritradar/commons/scaffold.dart';
import 'package:spritradar/stats/stats_charts.dart';

import 'stats_model.dart';

class StatsPage extends GetView<StatsControllerInterface> {
  @override
  Widget build(BuildContext context) => MyScaffold(
        onTab: (index) {
          if (index == 0) {
            if (controller.isLoggenIn) {
              Get.toNamed('/logbook');
            } else {
              Get.toNamed('/login');
            }
          }
          if (index == 1) Get.offAndToNamed('/home');
        },
        pageIndex: 2,
        child: Obx(
          () => Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text("Insert date range",
                    style:
                        TextStyle(fontSize: 32, fontWeight: FontWeight.bold)),
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(32.0),
                      child: TextField(
                        controller: controller.startController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'From',
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(32.0),
                      child: TextField(
                        controller: controller.endController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'To:',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: TextField(
                  controller: controller.stationController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'StationID (leave empty for all stations)',
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.black12,
                      textStyle: const TextStyle(fontSize: 20),
                    ),
                    onPressed: controller.runQuery,
                    child: const Text('Run query'),
                  ),
                  Spacer(),
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.black12,
                      textStyle: const TextStyle(fontSize: 20),
                    ),
                    onPressed: controller.fetchQuery,
                    child: const Text('Fetch statistics'),
                  ),
                  Spacer(),
                ],
              ),
              Divider(),
              Expanded(
                child: Container(
                  color: Colors.grey.shade200,
                  child: ListView(
                    children: controller.stats
                        .map(
                          (List<ChartData> chartData) => Padding(
                            padding: const EdgeInsets.all(32.0),
                            child: Container(
                              width: double.infinity,
                              height: 300,
                              child: Stack(children: [
                                SimpleTimeSeriesChart(chartData, true),
                                Align(
                                    alignment: Alignment.topRight,
                                    child: IconButton(
                                        onPressed: () => controller.deleteStat(
                                            statId: chartData.first.id),
                                        icon: Icon(Icons.delete))),
                              ]),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
              )
            ],
          ),
        ),
      );
}

abstract class StatsControllerInterface {
  TextEditingController get endController;
  TextEditingController get startController;
  TextEditingController get stationController;

  bool get isLoggenIn;
  Future<void> runQuery();
  RxList<List<ChartData>> get stats;

  void deleteStat({required String statId});

  void fetchQuery();
}
