import 'dart:convert';

import 'package:flutter/foundation.dart';

class Stats {
  final String id;
  final List<StatsModelPrice> petrolPrice;
  Stats({
    required this.id,
    required this.petrolPrice,
  });

  Stats copyWith({
    String? id,
    List<StatsModelPrice>? petrolPrice,
  }) {
    return Stats(
      id: id ?? this.id,
      petrolPrice: petrolPrice ?? this.petrolPrice,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'petrolPrice': petrolPrice.map((x) => x.toMap()).toList(),
    };
  }

  factory Stats.fromMap(Map<String, dynamic> map) {
    return Stats(
      id: map['id'] ?? '',
      petrolPrice: List<StatsModelPrice>.from(map['petrolPrice']?.map((x) => StatsModelPrice.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Stats.fromJson(String source) => Stats.fromMap(json.decode(source));

  @override
  String toString() => 'Stats(id: $id, petrolPrice: $petrolPrice)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Stats &&
      other.id == id &&
      listEquals(other.petrolPrice, petrolPrice);
  }

  @override
  int get hashCode => id.hashCode ^ petrolPrice.hashCode;
}

class StatsModelPrice {
  final String associatedStationI;
  final int dieselDeciCents;
  final int superE10DeciCents;
  final int superE5DeciCents;
  final String timestampIsoString;
  StatsModelPrice({
    required this.associatedStationI,
    required this.dieselDeciCents,
    required this.superE10DeciCents,
    required this.superE5DeciCents,
    required this.timestampIsoString,
  });

  StatsModelPrice copyWith({
    String? associatedStationI,
    int? dieselDeciCents,
    int? superE10DeciCents,
    int? superE5DeciCents,
    String? timestampIsoString,
  }) {
    return StatsModelPrice(
      associatedStationI: associatedStationI ?? this.associatedStationI,
      dieselDeciCents: dieselDeciCents ?? this.dieselDeciCents,
      superE10DeciCents: superE10DeciCents ?? this.superE10DeciCents,
      superE5DeciCents: superE5DeciCents ?? this.superE5DeciCents,
      timestampIsoString: timestampIsoString ?? this.timestampIsoString,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'associatedStationI': associatedStationI,
      'dieselDeciCents': dieselDeciCents,
      'superE10DeciCents': superE10DeciCents,
      'superE5DeciCents': superE5DeciCents,
      'timestampIsoString': timestampIsoString,
    };
  }

  factory StatsModelPrice.fromMap(Map<String, dynamic> map) {
    return StatsModelPrice(
      associatedStationI: map['associatedStationI'] ?? '',
      dieselDeciCents: map['dieselDeciCents']?.toInt() ?? 0,
      superE10DeciCents: map['superE10DeciCents']?.toInt() ?? 0,
      superE5DeciCents: map['superE5DeciCents']?.toInt() ?? 0,
      timestampIsoString: map['timestampIsoString'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory StatsModelPrice.fromJson(String source) => StatsModelPrice.fromMap(json.decode(source));

  @override
  String toString() {
    return 'StatsModelPrice(associatedStationI: $associatedStationI, dieselDeciCents: $dieselDeciCents, superE10DeciCents: $superE10DeciCents, superE5DeciCents: $superE5DeciCents, timestampIsoString: $timestampIsoString)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is StatsModelPrice &&
      other.associatedStationI == associatedStationI &&
      other.dieselDeciCents == dieselDeciCents &&
      other.superE10DeciCents == superE10DeciCents &&
      other.superE5DeciCents == superE5DeciCents &&
      other.timestampIsoString == timestampIsoString;
  }

  @override
  int get hashCode {
    return associatedStationI.hashCode ^
      dieselDeciCents.hashCode ^
      superE10DeciCents.hashCode ^
      superE5DeciCents.hashCode ^
      timestampIsoString.hashCode;
  }
}
