import 'package:get/get.dart';
import 'package:spritradar/services/rest_service.dart';

import 'logbook_controller.dart';
import 'logbook_page.dart';

class LogbookBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<LogbookControllerInterface>(LogbookController());
  }
}
