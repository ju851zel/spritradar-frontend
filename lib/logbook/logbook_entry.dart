import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:tuple/tuple.dart';

import '../theme.dart';

class RefuelLogbookEntry extends StatelessWidget {
  final String refuelLocation;
  final String refuelDate;
  final int mileage;
  final int refilledMilliliters;
  final int pricePerLiterDeziCent;
  final VoidCallback onPressed;
  final int totalPriceCents;

  const RefuelLogbookEntry({
    required this.refuelLocation,
    required this.refuelDate,
    required this.mileage,
    required this.refilledMilliliters,
    required this.pricePerLiterDeziCent,
    required this.totalPriceCents,
    required this.onPressed,
  });


  @override
  Widget build(BuildContext context) => _LogbookEntry(
        onPressed: onPressed,
        title: refuelLocation,
        description: refuelDate, // TODO: Format correct
        mainIcon: Icons.local_gas_station,
        infoIcons: [
          Tuple2(Icons.add_road, mileage.toString()),
          Tuple2(Icons.invert_colors,
              formatAmountLiters(amountMilliLiters: refilledMilliliters)),
          Tuple2(Icons.price_change,
              formatPricePerLiter(priceInDeziCents: pricePerLiterDeziCent)),
          Tuple2(Icons.paid, formatPrice(priceCents: totalPriceCents)),
        ],
      );
}

class RouteLogbookEntry extends StatelessWidget {
  final String routeStart;
  final String routeDestination;
  final String routeDate;
  final int routeLengthMeters;
  final int fuelConsumptionMilliLiters;
  final String driver;
  final int costsCents;
  final VoidCallback onPressed;

  const RouteLogbookEntry({
    required this.routeStart,
    required this.routeDestination,
    required this.routeLengthMeters,
    required this.fuelConsumptionMilliLiters,
    required this.routeDate,
    required this.driver,
    required this.costsCents,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) => _LogbookEntry(
        onPressed: onPressed,
        title: formatRouteJourney(),
        description: routeDate,
        mainIcon: Icons.map,
        infoIcons: [
          Tuple2(Icons.straighten,
              formatDistance(distanceMeters: routeLengthMeters)),
          Tuple2(
              Icons.account_balance_wallet_rounded,
              formatAmountLiters(
                  amountMilliLiters: fuelConsumptionMilliLiters)),
          Tuple2(Icons.person, driver),
          Tuple2(Icons.paid, formatPrice(priceCents: costsCents)),
        ],
      );

  String formatRouteJourney() => "$routeStart  --->  $routeDestination";
}

class _LogbookEntry extends StatelessWidget {
  final String title;
  final String description;
  final IconData mainIcon;
  final VoidCallback onPressed;
  final List<Tuple2<IconData, String>> infoIcons;

  const _LogbookEntry({
    required this.title,
    required this.description,
    required this.mainIcon,
    required this.infoIcons,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.grey.shade400,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                mainIcon,
                size: 48,
              ),
              Container(width: 8),
              Expanded(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(title, style: AppTextStyles.heading),
                          Text(description),
                        ],
                      ),
                    ),
                    buildInfoIcons(),
                  ],
                ),
              ),
              IconButton(onPressed: onPressed, icon: Icon(Icons.delete)),
            ],
          ),
        ),
      );

  Widget buildInfoIcons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: infoIcons
          .map((tuple) => buildIconWithText(
                icon: tuple.item1,
                description: tuple.item2,
              ))
          .toList(),
    );
  }

  Widget buildIconWithText({
    required IconData icon,
    required String description,
  }) =>
      Flexible(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(icon),
            Text(description),
          ],
        ),
      );
}

String formatDate({required DateTime date}) =>
    new DateFormat('dd-MM-yyyy, HH:mm').format(date);

String formatPrice({required int priceCents}) =>
    (priceCents / 100).toStringAsFixed(2);

String formatPricePerLiter({required int priceInDeziCents}) =>
    (priceInDeziCents / 100).toStringAsFixed(3);

String formatAmountLiters({required int amountMilliLiters}) =>
    (amountMilliLiters / 1000).toStringAsFixed(2);

String formatDistance({required int distanceMeters}) =>
    (distanceMeters / 1000).toStringAsFixed(1);
