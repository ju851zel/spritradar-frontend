import 'dart:convert';
import 'dart:html' as html;
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

import 'logbook_page.dart';
import 'logbook_page_model.dart';

class LogbookController extends GetxController
    implements LogbookControllerInterface {
  final RxList<LogbookPageModelGroup> _vehicles = RxList();
  Rx<String?> _selectedVehicleId = Rx(null);
  TextEditingController _joinGroupBrandTextController = TextEditingController();
  TextEditingController _createVehicleBrandTextController =
      TextEditingController();
  TextEditingController _createVehicleNameTextController =
      TextEditingController();
  TextEditingController _routeEntryConsumptionTextController =
      TextEditingController();
  TextEditingController _routeEntryCostsTextController =
      TextEditingController();
  TextEditingController _routeEntryDepartureTextController =
      TextEditingController();
  TextEditingController _routeEntryDestinationTextController =
      TextEditingController();
  TextEditingController _routeEntryDistanceTextController =
      TextEditingController();
  TextEditingController _refuelEntryConsumptionTextController =
      TextEditingController();
  TextEditingController _refuelEntryCostsTextController =
      TextEditingController();
  TextEditingController _refuelEntryMileageTextController =
      TextEditingController();
  TextEditingController _refuelEntryLiterCostTextController =
      TextEditingController();
  TextEditingController _refuelEntryPositionTextController =
      TextEditingController();

  LogbookRestService service = Get.find();

  @override
  RxList<LogbookPageModelGroup> get vehicles => _vehicles;

  @override
  Rx<String?> get selectedVehicleId => _selectedVehicleId;

  @override
  Future<void> onInit() async {
    super.onInit();
    fetchVehicles();
  }

  @override
  Future<void> fetchVehicles() async {
    final List<LogbookRestServiceGroups> fetchedVehicles =
        (await service.fetchVehicles());
    _vehicles.value = await Future.wait(fetchedVehicles.map((eintrag) async {
      var vecs = (await Future.wait(eintrag.vehicles.map((vehicle) async {
        final routeEntries = await service.fetchRouteEntries(
          groupId: eintrag.groupId,
          vehicleId: vehicle.id,
        );
        final refuelEntries = await service.fetchRefuelEntries(
          groupId: eintrag.groupId,
          vehicleId: vehicle.id,
        );
        return LogbookPageModelVehicle(
          id: vehicle.id,
          name: vehicle.name,
          brand: vehicle.brand,
          refuelEntries:
              refuelEntries.map((it) => mapTypeRefuel(entry: it)).toList(),
          routeEntries:
              routeEntries.map((it) => mapTypeRoute(entry: it)).toList(),
        );
      })));
      return LogbookPageModelGroup(
          groupID: eintrag.groupId, name: eintrag.name, vehicles: vecs);
    }));
    print("fetched vehicles: $_vehicles.value");
  }

  @override
  Future<bool> logout() async {
    if (await service.logout()) {
      Get.offAndToNamed('/login');
    }
    return true;
  }

  @override
  Future<void> generateInvoice() async {
    if (service.isEnterprise == null ||
        service.groupId != service.isEnterprise) {
      Fluttertoast.showToast(
          msg: "Only for enterprise customers\nif you are enterprise, wait for your instance to be deployed",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    final pdf = pw.Document();

    var data = _vehicles
        .map((tuple) {
          return tuple.vehicles
              .map((e) => e.routeEntries)
              .toList()
              .flattened
              .map((e) => [
                    e.name,
                    e.departure,
                    e.destination,
                    "${e.totalCostsCent / 100} Euro"
                  ])
              .toList();
        })
        .flattened
        .toList();

    print(data);
    pdf.addPage(pw.Page(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return pw.Table.fromTextArray(
            border: null,
            cellAlignment: pw.Alignment.centerLeft,
            headerDecoration: pw.BoxDecoration(
                borderRadius: const pw.BorderRadius.all(pw.Radius.circular(2)),
                color: PdfColors.blue200),
            headerHeight: 25,
            cellHeight: 40,
            cellAlignments: {
              0: pw.Alignment.centerLeft,
              1: pw.Alignment.centerLeft,
              2: pw.Alignment.centerRight,
              3: pw.Alignment.center,
              4: pw.Alignment.centerRight,
            },
            headerStyle: pw.TextStyle(
              color: PdfColors.black,
              fontSize: 10,
              fontWeight: pw.FontWeight.bold,
            ),
            cellStyle: const pw.TextStyle(
              color: PdfColors.black,
              fontSize: 10,
            ),
            rowDecoration: pw.BoxDecoration(
              border: pw.Border(
                bottom: pw.BorderSide(
                  color: PdfColors.black,
                  width: .5,
                ),
              ),
            ),
            headers: ["Name", "From", "To", "Cost"],
            data: data,
          );
        })); // P
    final bytes = await pdf.save();
    final blob = html.Blob([bytes], 'application/pdf');
    final url = html.Url.createObjectUrlFromBlob(blob);
    html.window.open(url, "_blank");
    html.Url.revokeObjectUrl(url);

    // final url = html.Url.createObjectUrlFromBlob(blob);
    // final anchor = html.document.createElement('a') as html.AnchorElement
    //   ..href = url
    //   ..style.display = 'none'
    //   ..download = 'some_name.pdf';
    // html.document.body?.children.add(anchor);
    // anchor.click();
    // html.document.body?.children.remove(anchor);
    // html.Url.revokeObjectUrl(url);
  }

  @override
  Future<void> createVehicle() async {
    int countVehicles = _vehicles
              .firstWhereOrNull((element) =>
                  element.vehicles.firstWhereOrNull(
                      (vehicle) => vehicle.id == _selectedVehicleId.value) !=
                  null)
              ?.vehicles.length ?? 0;
    if (countVehicles >= 3) {
      Fluttertoast.showToast(
          msg: "You can create 3 cars",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    bool success = await service.createVehicle(
      name: _createVehicleNameTextController.text,
      brand: _createVehicleBrandTextController.text,
    );
    Get.close(1);
    if (!success) {
      Fluttertoast.showToast(
          msg: "Could not create vehicles :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
    _createVehicleNameTextController.text = "";
    _createVehicleBrandTextController.text = "";
    print("Createing vehicle");
  }

  @override
  TextEditingController get createVehicleBrandTextController =>
      _createVehicleBrandTextController;

  @override
  TextEditingController get createVehicleNameTextController =>
      _createVehicleNameTextController;

  @override
  TextEditingController get routeEntryConsumptionTextController =>
      _routeEntryConsumptionTextController;

  @override
  TextEditingController get routeEntryCostsTextController =>
      _routeEntryCostsTextController;

  @override
  TextEditingController get routeEntryDepartureTextController =>
      _routeEntryDepartureTextController;

  @override
  TextEditingController get routeEntryDestinationTextController =>
      _routeEntryDestinationTextController;

  @override
  TextEditingController get routeEntryDistanceTextController =>
      _routeEntryDistanceTextController;

  @override
  TextEditingController get refuelEntryConsumptionTextController =>
      _refuelEntryConsumptionTextController;

  @override
  TextEditingController get refuelEntryCostsTextController =>
      _refuelEntryCostsTextController;

  @override
  TextEditingController get refuelEntryMileageTextController =>
      _refuelEntryMileageTextController;

  @override
  TextEditingController get refuelEntryLiterCostTextController =>
      _refuelEntryLiterCostTextController;

  @override
  TextEditingController get refuelEntryPositionTextController =>
      _refuelEntryPositionTextController;

  @override
  void createRouteEntry() async {
    if (!(await service.createRouteEntry(
      selectedVehicleId: _selectedVehicleId.value!,
      departure: _routeEntryDepartureTextController.text,
      destination: _routeEntryDestinationTextController.text,
      distanceMeters:
          (int.parse(_routeEntryDistanceTextController.text) * 1000).toString(),
      consumptionMilliliter:
          (double.parse(_routeEntryConsumptionTextController.text) * 1000)
              .toInt(),
      totalCostsCent:
          (double.parse(_routeEntryCostsTextController.text) * 100).toInt(),
      groupID: _vehicles
              .firstWhereOrNull((element) =>
                  element.vehicles.firstWhereOrNull(
                      (vehicle) => vehicle.id == _selectedVehicleId.value) !=
                  null)
              ?.groupID ??
          "",
      name: _vehicles
              .firstWhereOrNull((element) => element.groupID == service.groupId)
              ?.name ??
          "",
    ))) {
      Fluttertoast.showToast(
          msg: "Could not create route entry :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  void createRefuelEntry() async {
    if (!(await service.createRefuelEntry(
        selectedVehicleId: _selectedVehicleId.value!,
        position: _refuelEntryPositionTextController.text,
        mileageMeters:
            (int.parse(_refuelEntryMileageTextController.text) * 1000)
                .toString(),
        consumptionMilliliter:
            (double.parse(_refuelEntryConsumptionTextController.text) * 1000)
                .toInt(),
        pricePerLiter:
            (double.parse(_refuelEntryLiterCostTextController.text) * 100)
                .toInt(),
        totalCostsCent:
            (double.parse(_refuelEntryCostsTextController.text) * 100).toInt(),
        groupID: _vehicles
                .firstWhereOrNull((element) =>
                    element.vehicles.firstWhereOrNull(
                        (vehicle) => vehicle.id == _selectedVehicleId.value) !=
                    null)
                ?.groupID ??
            "",
        name: _vehicles.firstWhereOrNull((element) => element.vehicles.firstWhereOrNull((vehicle) => vehicle.id == _selectedVehicleId.value) != null)?.name ?? ""))) {
      Fluttertoast.showToast(
          msg: "Could not create refuel entry :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  void setSelectedVehicle({required String id}) {
    _selectedVehicleId.value = id;
  }

  @override
  String? get ownGroupId => service.groupId;

  @override
  void joinGroup() async {
    if (!(await service.joinGroup(
        currentGroupId: _joinGroupBrandTextController.text))) {
      Fluttertoast.showToast(
          msg: "Could not join group :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  TextEditingController get joinGroupBrandTextController =>
      _joinGroupBrandTextController;

  @override
  Future<void> leaveGroup({required String groupId}) async {
    if (!await service.leaveGroup(groupId: groupId)) {
      Fluttertoast.showToast(
          msg: "Could not leave group :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  void deleteVehicle() async {
    var groupID = _vehicles
        .firstWhereOrNull((element) =>
            element.vehicles.firstWhereOrNull(
                (vehicle) => vehicle.id == _selectedVehicleId.value) !=
            null)
        ?.groupID;
    if (groupID != this.service.groupId) {
      Fluttertoast.showToast(
          msg: "Can't delete a vehicle you are not owner of",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    if (!await service.deleteVehicle(
        groupID: groupID, vehicleID: _selectedVehicleId.value)) {
      Fluttertoast.showToast(
          msg: "Could not delete vehicle :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  void deleteRefuelEntry(String id) async {
    if (!await service.deleteRefuelEntry(
      entryId: id,
      vehicleId: selectedVehicleId.value ?? "",
      groupId: _vehicles
              .firstWhereOrNull((element) =>
                  element.vehicles.firstWhereOrNull(
                      (vehicle) => vehicle.id == _selectedVehicleId.value) !=
                  null)
              ?.groupID ??
          "",
    )) {
      Fluttertoast.showToast(
          msg: "Could not delete entry :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  void deleteRouteEntry(String id) async {
    if (!await service.deleteRouteEntry(
      entryId: id,
      vehicleId: selectedVehicleId.value ?? "",
      groupId: _vehicles
              .firstWhereOrNull((element) =>
                  element.vehicles.firstWhereOrNull(
                      (vehicle) => vehicle.id == _selectedVehicleId.value) !=
                  null)
              ?.groupID ??
          "",
    )) {
      Fluttertoast.showToast(
          msg: "Could not delete entry :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    fetchVehicles();
  }

  @override
  bool get isLoggenInLogbook => service.isLoggenInLogbook;
}

LogbookPageModelRefuelEntry mapTypeRefuel({
  required LogbookRestServiceModelRefuelEntry entry,
}) =>
    LogbookPageModelRefuelEntry(
      id: entry.id,
      position: entry.position,
      distanceMeters: entry.mileageMeters,
      consumptionMilliliter: entry.consumptionMilliliter,
      pricePerLiterCents: entry.pricePerLiterCents,
      totalCostsCent: entry.totalCostsCent,
      creationDateIsoString: entry.creationDateIsoString,
    );

LogbookPageModelRouteEntry mapTypeRoute({
  required LogbookRestServiceModelRouteEntry entry,
}) =>
    LogbookPageModelRouteEntry(
      id: entry.id,
      departure: entry.departure,
      destination: entry.destination,
      name: entry.name,
      distanceMeters: entry.distanceMeters,
      consumptionMilliliter: entry.consumptionMilliliter,
      totalCostsCent: entry.totalCostsCent,
      creationDateIsoString: entry.creationDateIsoString,
    );

abstract class LogbookRestService {
  String? get groupId;

  String? get isEnterprise;

  bool get isLoggenInLogbook;

  Future<List<LogbookRestServiceGroups>> fetchVehicles();

  Future<List<LogbookRestServiceModelRouteEntry>> fetchRouteEntries({
    required String groupId,
    required String vehicleId,
  });

  Future<List<LogbookRestServiceModelRefuelEntry>> fetchRefuelEntries({
    required String groupId,
    required String vehicleId,
  });

  Future<bool> logout();

  Future<bool> createVehicle({required String name, required String brand});

  Future<bool> createRouteEntry({
    required String selectedVehicleId,
    required String departure,
    required String destination,
    required String distanceMeters,
    required int consumptionMilliliter,
    required int totalCostsCent,
    required String groupID,
    required String name,
  });

  Future<bool> leaveGroup({required String groupId});

  Future<bool> joinGroup({required String currentGroupId});

  Future<bool> deleteVehicle({
    String? groupID,
    String? vehicleID,
  });

  Future<bool> createRefuelEntry({
    required String selectedVehicleId,
    required String position,
    required String mileageMeters,
    required int consumptionMilliliter,
    required int pricePerLiter,
    required int totalCostsCent,
    required String groupID,
    required String name,
  });

  Future<bool> deleteRefuelEntry({
    required String entryId,
    required String vehicleId,
    required String groupId,
  });
  Future<bool> deleteRouteEntry({
    required String entryId,
    required String vehicleId,
    required String groupId,
  });
}

class LogbookRestServiceModelRefuelEntry {
  final String id;
  final String position;
  final String mileageMeters;
  final int consumptionMilliliter;
  final int pricePerLiterCents;
  final int totalCostsCent;
  final String creationDateIsoString;
  LogbookRestServiceModelRefuelEntry({
    required this.id,
    required this.position,
    required this.mileageMeters,
    required this.consumptionMilliliter,
    required this.pricePerLiterCents,
    required this.totalCostsCent,
    required this.creationDateIsoString,
  });

  LogbookRestServiceModelRefuelEntry copyWith({
    String? id,
    String? position,
    String? distanceMeters,
    int? consumptionMilliliter,
    int? pricePerLiterCents,
    int? totalCostsCent,
    String? creationDateIsoString,
  }) {
    return LogbookRestServiceModelRefuelEntry(
      id: id ?? this.id,
      position: position ?? this.position,
      mileageMeters: distanceMeters ?? this.mileageMeters,
      consumptionMilliliter:
          consumptionMilliliter ?? this.consumptionMilliliter,
      pricePerLiterCents: pricePerLiterCents ?? this.pricePerLiterCents,
      totalCostsCent: totalCostsCent ?? this.totalCostsCent,
      creationDateIsoString:
          creationDateIsoString ?? this.creationDateIsoString,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'position': position,
      'mileageMeters': mileageMeters,
      'consumptionMilliliter': consumptionMilliliter,
      'pricePerLiterCents': pricePerLiterCents,
      'totalCostsCent': totalCostsCent,
      'creationDateIsoString': creationDateIsoString,
    };
  }

  factory LogbookRestServiceModelRefuelEntry.fromMap(Map<String, dynamic> map) {
    return LogbookRestServiceModelRefuelEntry(
      id: map['id'] ?? '',
      position: map['position'] ?? '',
      mileageMeters: map['mileageMeters'] ?? '',
      consumptionMilliliter: map['consumptionMilliliter']?.toInt() ?? 0,
      pricePerLiterCents: map['pricePerLiterCents']?.toInt() ?? 0,
      totalCostsCent: map['totalCostsCent']?.toInt() ?? 0,
      creationDateIsoString: map['creationDateIsoString'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookRestServiceModelRefuelEntry.fromJson(String source) =>
      LogbookRestServiceModelRefuelEntry.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LogbookRestServiceModelRefuelEntry(id: $id, position: $position, mileageMeters: $mileageMeters, consumptionMilliliter: $consumptionMilliliter, pricePerLiterCents: $pricePerLiterCents, totalCostsCent: $totalCostsCent, creationDateIsoString: $creationDateIsoString)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookRestServiceModelRefuelEntry &&
        other.id == id &&
        other.position == position &&
        other.mileageMeters == mileageMeters &&
        other.consumptionMilliliter == consumptionMilliliter &&
        other.pricePerLiterCents == pricePerLiterCents &&
        other.totalCostsCent == totalCostsCent &&
        other.creationDateIsoString == creationDateIsoString;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        position.hashCode ^
        mileageMeters.hashCode ^
        consumptionMilliliter.hashCode ^
        pricePerLiterCents.hashCode ^
        totalCostsCent.hashCode ^
        creationDateIsoString.hashCode;
  }
}

class LogbookRestServiceModelRouteEntry {
  final String id;
  final String departure;
  final String destination;
  final String name;
  final String distanceMeters;
  final int consumptionMilliliter;
  final int totalCostsCent;
  final String creationDateIsoString;
  LogbookRestServiceModelRouteEntry({
    required this.id,
    required this.departure,
    required this.destination,
    required this.name,
    required this.distanceMeters,
    required this.consumptionMilliliter,
    required this.totalCostsCent,
    required this.creationDateIsoString,
  });

  LogbookRestServiceModelRouteEntry copyWith({
    String? id,
    String? departure,
    String? destination,
    String? name,
    String? distanceMeters,
    int? consumptionMilliliter,
    int? totalCostsCent,
    String? creationDateIsoString,
  }) {
    return LogbookRestServiceModelRouteEntry(
      id: id ?? this.id,
      departure: departure ?? this.departure,
      destination: destination ?? this.destination,
      name: name ?? this.name,
      distanceMeters: distanceMeters ?? this.distanceMeters,
      consumptionMilliliter:
          consumptionMilliliter ?? this.consumptionMilliliter,
      totalCostsCent: totalCostsCent ?? this.totalCostsCent,
      creationDateIsoString:
          creationDateIsoString ?? this.creationDateIsoString,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'departure': departure,
      'destination': destination,
      'name': name,
      'distanceMeters': distanceMeters,
      'consumptionMilliliter': consumptionMilliliter,
      'totalCostsCent': totalCostsCent,
      'creationDateIsoString': creationDateIsoString,
    };
  }

  factory LogbookRestServiceModelRouteEntry.fromMap(Map<String, dynamic> map) {
    return LogbookRestServiceModelRouteEntry(
      id: map['id'] ?? '',
      departure: map['departure'] ?? '',
      destination: map['destination'] ?? '',
      name: map['name'] ?? '',
      distanceMeters: map['distanceMeters'] ?? '',
      consumptionMilliliter: map['consumptionMilliliter']?.toInt() ?? 0,
      totalCostsCent: map['totalCostsCent']?.toInt() ?? 0,
      creationDateIsoString: map['creationDateIsoString'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookRestServiceModelRouteEntry.fromJson(String source) =>
      LogbookRestServiceModelRouteEntry.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LogbookRestServiceModelRouteEntry(id: $id, departure: $departure, destination: $destination, name: $name, distanceMeters: $distanceMeters, consumptionMilliliter: $consumptionMilliliter, totalCostsCent: $totalCostsCent, creationDateIsoString: $creationDateIsoString)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookRestServiceModelRouteEntry &&
        other.id == id &&
        other.departure == departure &&
        other.destination == destination &&
        other.name == name &&
        other.distanceMeters == distanceMeters &&
        other.consumptionMilliliter == consumptionMilliliter &&
        other.totalCostsCent == totalCostsCent &&
        other.creationDateIsoString == creationDateIsoString;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        departure.hashCode ^
        destination.hashCode ^
        name.hashCode ^
        distanceMeters.hashCode ^
        consumptionMilliliter.hashCode ^
        totalCostsCent.hashCode ^
        creationDateIsoString.hashCode;
  }
}

class LogbookRestServiceGroups {
  final String groupId;
  final String name;
  final List<LogbookRestServiceModelVehicle> vehicles;

  LogbookRestServiceGroups({
    required this.groupId,
    required this.name,
    required this.vehicles,
  });

  LogbookRestServiceGroups copyWith({
    String? groupId,
    String? name,
    List<LogbookRestServiceModelVehicle>? vehicles,
  }) {
    return LogbookRestServiceGroups(
      groupId: groupId ?? this.groupId,
      name: name ?? this.name,
      vehicles: vehicles ?? this.vehicles,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'groupId': groupId,
      'name': name,
      'vehicles': vehicles.map((x) => x.toMap()).toList(),
    };
  }

  factory LogbookRestServiceGroups.fromMap(Map<String, dynamic> map) {
    return LogbookRestServiceGroups(
      groupId: map['groupId'] ?? '',
      name: map['name'] ?? '',
      vehicles: List<LogbookRestServiceModelVehicle>.from(map['vehicles']
          ?.map((x) => LogbookRestServiceModelVehicle.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookRestServiceGroups.fromJson(String source) =>
      LogbookRestServiceGroups.fromMap(json.decode(source));

  @override
  String toString() =>
      'LogbookRestServiceGroups(groupId: $groupId, name: $name, vehicles: $vehicles)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookRestServiceGroups &&
        other.groupId == groupId &&
        other.name == name &&
        listEquals(other.vehicles, vehicles);
  }

  @override
  int get hashCode => groupId.hashCode ^ name.hashCode ^ vehicles.hashCode;
}

class LogbookRestServiceModelVehicle {
  final String id;
  final String name;
  final String brand;
  LogbookRestServiceModelVehicle({
    required this.id,
    required this.name,
    required this.brand,
  });

  LogbookRestServiceModelVehicle copyWith({
    String? id,
    String? name,
    String? brand,
  }) {
    return LogbookRestServiceModelVehicle(
      id: id ?? this.id,
      name: name ?? this.name,
      brand: brand ?? this.brand,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'brand': brand,
    };
  }

  factory LogbookRestServiceModelVehicle.fromMap(Map<String, dynamic> map) {
    return LogbookRestServiceModelVehicle(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      brand: map['brand'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookRestServiceModelVehicle.fromJson(String source) =>
      LogbookRestServiceModelVehicle.fromMap(json.decode(source));

  @override
  String toString() =>
      'LogbookRestServiceModelVehicle(id: $id, name: $name, brand: $brand)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookRestServiceModelVehicle &&
        other.id == id &&
        other.name == name &&
        other.brand == brand;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ brand.hashCode;
}
