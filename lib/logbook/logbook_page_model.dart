import 'dart:convert';

import 'package:flutter/foundation.dart';

class LogbookPageModelGroup {
  final String groupID;
  final String name;
  final List<LogbookPageModelVehicle> vehicles;
  LogbookPageModelGroup({
    required this.groupID,
    required this.name,
    required this.vehicles,
  });

  LogbookPageModelGroup copyWith({
    String? id,
    String? name,
    List<LogbookPageModelVehicle>? vehicles,
  }) {
    return LogbookPageModelGroup(
      groupID: id ?? this.groupID,
      name: name ?? this.name,
      vehicles: vehicles ?? this.vehicles,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': groupID,
      'name': name,
      'vehicles': vehicles.map((x) => x.toMap()).toList(),
    };
  }

  factory LogbookPageModelGroup.fromMap(Map<String, dynamic> map) {
    return LogbookPageModelGroup(
      groupID: map['id'] ?? '',
      name: map['name'] ?? '',
      vehicles: List<LogbookPageModelVehicle>.from(map['vehicles']?.map((x) => LogbookPageModelVehicle.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookPageModelGroup.fromJson(String source) => LogbookPageModelGroup.fromMap(json.decode(source));

  @override
  String toString() => 'LogbookPageModelGroup(id: $groupID, name: $name, vehicles: $vehicles)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is LogbookPageModelGroup &&
      other.groupID == groupID &&
      other.name == name &&
      listEquals(other.vehicles, vehicles);
  }

  @override
  int get hashCode => groupID.hashCode ^ name.hashCode ^ vehicles.hashCode;
}

class LogbookPageModelVehicle {
  final String id;
  final String name;
  final String brand;
  final List<LogbookPageModelRefuelEntry> refuelEntries;
  final List<LogbookPageModelRouteEntry> routeEntries;
  LogbookPageModelVehicle({
    required this.id,
    required this.name,
    required this.brand,
    required this.refuelEntries,
    required this.routeEntries,
  });

  LogbookPageModelVehicle copyWith({
    String? id,
    String? name,
    String? brand,
    List<LogbookPageModelRefuelEntry>? refuelEntries,
    List<LogbookPageModelRouteEntry>? routeEntries,
  }) {
    return LogbookPageModelVehicle(
      id: id ?? this.id,
      name: name ?? this.name,
      brand: brand ?? this.brand,
      refuelEntries: refuelEntries ?? this.refuelEntries,
      routeEntries: routeEntries ?? this.routeEntries,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'brand': brand,
      'refuelEntries': refuelEntries.map((x) => x.toMap()).toList(),
      'routeEntries': routeEntries.map((x) => x.toMap()).toList(),
    };
  }

  factory LogbookPageModelVehicle.fromMap(Map<String, dynamic> map) {
    return LogbookPageModelVehicle(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      brand: map['brand'] ?? '',
      refuelEntries: List<LogbookPageModelRefuelEntry>.from(map['refuelEntries']
          ?.map((x) => LogbookPageModelRefuelEntry.fromMap(x))),
      routeEntries: List<LogbookPageModelRouteEntry>.from(map['routeEntries']
          ?.map((x) => LogbookPageModelRouteEntry.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookPageModelVehicle.fromJson(String source) =>
      LogbookPageModelVehicle.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LogbookPageModelVehicle(id: $id, name: $name, brand: $brand, refuelEntries: $refuelEntries, routeEntries: $routeEntries)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookPageModelVehicle &&
        other.id == id &&
        other.name == name &&
        other.brand == brand &&
        listEquals(other.refuelEntries, refuelEntries) &&
        listEquals(other.routeEntries, routeEntries);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        brand.hashCode ^
        refuelEntries.hashCode ^
        routeEntries.hashCode;
  }
}

class LogbookPageModelRefuelEntry {
  final String id;
  final String position;
  final String distanceMeters;
  final int consumptionMilliliter;
  final int pricePerLiterCents;
  final int totalCostsCent;
  final String creationDateIsoString;
  LogbookPageModelRefuelEntry({
    required this.id,
    required this.position,
    required this.distanceMeters,
    required this.consumptionMilliliter,
    required this.pricePerLiterCents,
    required this.totalCostsCent,
    required this.creationDateIsoString,
  });

  LogbookPageModelRefuelEntry copyWith({
    String? id,
    String? position,
    String? distanceMeters,
    int? consumptionMilliliter,
    int? pricePerLiterCents,
    int? totalCostsCent,
    String? creationDateIsoString,
  }) {
    return LogbookPageModelRefuelEntry(
      id: id ?? this.id,
      position: position ?? this.position,
      distanceMeters: distanceMeters ?? this.distanceMeters,
      consumptionMilliliter:
          consumptionMilliliter ?? this.consumptionMilliliter,
      pricePerLiterCents: pricePerLiterCents ?? this.pricePerLiterCents,
      totalCostsCent: totalCostsCent ?? this.totalCostsCent,
      creationDateIsoString:
          creationDateIsoString ?? this.creationDateIsoString,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'position': position,
      'distanceMeters': distanceMeters,
      'consumptionMilliliter': consumptionMilliliter,
      'pricePerLiterCents': pricePerLiterCents,
      'totalCostsCent': totalCostsCent,
      'creationDateIsoString': creationDateIsoString,
    };
  }

  factory LogbookPageModelRefuelEntry.fromMap(Map<String, dynamic> map) {
    return LogbookPageModelRefuelEntry(
      id: map['id'] ?? '',
      position: map['position'] ?? '',
      distanceMeters: map['distanceMeters'] ?? '',
      consumptionMilliliter: map['consumptionMilliliter']?.toInt() ?? 0,
      pricePerLiterCents: map['pricePerLiterCents']?.toInt() ?? 0,
      totalCostsCent: map['totalCostsCent']?.toInt() ?? 0,
      creationDateIsoString: map['creationDateIsoString'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookPageModelRefuelEntry.fromJson(String source) =>
      LogbookPageModelRefuelEntry.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LogbookPageModelRefuelEntry(id: $id, position: $position, distanceMeters: $distanceMeters, consumptionMilliliter: $consumptionMilliliter, pricePerLiterCents: $pricePerLiterCents, totalCostsCent: $totalCostsCent, creationDateIsoString: $creationDateIsoString)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookPageModelRefuelEntry &&
        other.id == id &&
        other.position == position &&
        other.distanceMeters == distanceMeters &&
        other.consumptionMilliliter == consumptionMilliliter &&
        other.pricePerLiterCents == pricePerLiterCents &&
        other.totalCostsCent == totalCostsCent &&
        other.creationDateIsoString == creationDateIsoString;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        position.hashCode ^
        distanceMeters.hashCode ^
        consumptionMilliliter.hashCode ^
        pricePerLiterCents.hashCode ^
        totalCostsCent.hashCode ^
        creationDateIsoString.hashCode;
  }
}

class LogbookPageModelRouteEntry {
  final String id;
  final String departure;
  final String destination;
  final String name;
  final String distanceMeters;
  final int consumptionMilliliter;
  final int totalCostsCent;
  final String creationDateIsoString;
  LogbookPageModelRouteEntry({
    required this.id,
    required this.departure,
    required this.destination,
    required this.name,
    required this.distanceMeters,
    required this.consumptionMilliliter,
    required this.totalCostsCent,
    required this.creationDateIsoString,
  });

  LogbookPageModelRouteEntry copyWith({
    String? id,
    String? departure,
    String? destination,
    String? name,
    String? distanceMeters,
    int? consumptionMilliliter,
    int? totalCostsCent,
    String? creationDateIsoString,
  }) {
    return LogbookPageModelRouteEntry(
      id: id ?? this.id,
      departure: departure ?? this.departure,
      destination: destination ?? this.destination,
      name: name ?? this.name,
      distanceMeters: distanceMeters ?? this.distanceMeters,
      consumptionMilliliter:
          consumptionMilliliter ?? this.consumptionMilliliter,
      totalCostsCent: totalCostsCent ?? this.totalCostsCent,
      creationDateIsoString:
          creationDateIsoString ?? this.creationDateIsoString,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'departure': departure,
      'destination': destination,
      'name': name,
      'distanceMeters': distanceMeters,
      'consumptionMilliliter': consumptionMilliliter,
      'totalCostsCent': totalCostsCent,
      'creationDateIsoString': creationDateIsoString,
    };
  }

  factory LogbookPageModelRouteEntry.fromMap(Map<String, dynamic> map) {
    return LogbookPageModelRouteEntry(
      id: map['id'] ?? '',
      departure: map['departure'] ?? '',
      destination: map['destination'] ?? '',
      name: map['name'] ?? '',
      distanceMeters: map['distanceMeters'] ?? '',
      consumptionMilliliter: map['consumptionMilliliter']?.toInt() ?? 0,
      totalCostsCent: map['totalCostsCent']?.toInt() ?? 0,
      creationDateIsoString: map['creationDateIsoString'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LogbookPageModelRouteEntry.fromJson(String source) =>
      LogbookPageModelRouteEntry.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LogbookPageModelRouteEntry(id: $id, departure: $departure, destination: $destination, name: $name, distanceMeters: $distanceMeters, consumptionMilliliter: $consumptionMilliliter, totalCostsCent: $totalCostsCent, creationDateIsoString: $creationDateIsoString)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LogbookPageModelRouteEntry &&
        other.id == id &&
        other.departure == departure &&
        other.destination == destination &&
        other.name == name &&
        other.distanceMeters == distanceMeters &&
        other.consumptionMilliliter == consumptionMilliliter &&
        other.totalCostsCent == totalCostsCent &&
        other.creationDateIsoString == creationDateIsoString;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        departure.hashCode ^
        destination.hashCode ^
        name.hashCode ^
        distanceMeters.hashCode ^
        consumptionMilliliter.hashCode ^
        totalCostsCent.hashCode ^
        creationDateIsoString.hashCode;
  }
}
