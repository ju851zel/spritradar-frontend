import 'package:flutter/material.dart';

class LogbookVehicle extends StatelessWidget {
  final String name;
  final Color color;
  final VoidCallback onClick;
  final bool active;
  final String? groupName;

  const LogbookVehicle({
    Key? key,
    required this.color,
    required this.name,
    required this.onClick,
    required this.active,
    required this.groupName,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        width: 120,
        height: 120,
        decoration: BoxDecoration(
          color: active ? Colors.amberAccent: color,
          shape: BoxShape.circle,
        ),
        child: Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(name, style: TextStyle(fontSize: 16),),
            if (groupName == null) Text("(mine)", style: TextStyle(fontSize: 12)),
            if (groupName != null) Text("($groupName)", style: TextStyle(fontSize: 12)),
          ],
        )),
      ),
    );
  }
}

class LogbookAddButtonVehicle extends StatelessWidget {
  final VoidCallback onClick;

  const LogbookAddButtonVehicle({Key? key, required this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        width: 80,
        height: 80,
        decoration: BoxDecoration(
          color: Colors.blue,
          shape: BoxShape.circle,
        ),
        child: Icon(Icons.add, size: 34),
      ),
    );
  }
}
