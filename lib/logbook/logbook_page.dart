import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:spritradar/commons/scaffold.dart';
import 'package:spritradar/logbook/avatar.dart';
import 'package:spritradar/logbook/logbook_vehicle.dart';
import 'package:tuple/tuple.dart';
import 'dart:math' as math;
import 'package:collection/collection.dart';

import 'logbook_entry.dart';
import 'logbook_page_model.dart';

class LogbookPage extends GetView<LogbookControllerInterface> {
  @override
  Widget build(BuildContext context) => Stack(
        children: [
          MyScaffold(
            pageIndex: 0,
            onTab: (index) {
              if (index == 1) Get.offAndToNamed('/home');
              if (index == 2) {
                if (controller.isLoggenInLogbook) {
                  Get.offAndToNamed('/stats');
                } else {
                  Get.toNamed('/login');
                }
              }
            },
            child: Column(
              children: [
                Expanded(flex: 2, child: buildVehiclesRow()),
                Expanded(
                  flex: 7,
                  child: Container(
                    color: Colors.black12,
                    child: Obx(
                      () => ListView(children: [
                        ...getCurrentRefuelEntries(),
                        ...getCurrentRouteEntries(),
                      ]),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Obx(
              () => Wrap(
                children: [
                  Avatar(
                    onTab: showGroupsDialog,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(
                        'New entry',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: (controller.vehicles.isEmpty ||
                              controller.selectedVehicleId.value == null)
                          ? null
                          : showAddEntryDialog,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(
                        'Logout',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: controller.logout,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(
                        'Fetch Vehicles',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: controller.fetchVehicles,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(
                        'Delete selected vehicle',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: controller.selectedVehicleId.value == null
                          ? null
                          : controller.deleteVehicle,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(
                        'Create invoice',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: () => controller.generateInvoice(),
                    ),
                  ),
                  Container(width: 20),
                ],
              ),
            ),
          )
        ],
      );

  List<Widget> getCurrentRefuelEntries() {
    return controller.vehicles
            .map((element) => element.vehicles)
            .toList()
            .expand((e) => e)
            .toList()
            .firstWhereOrNull(
                (car) => car.id == controller.selectedVehicleId.value)
            ?.refuelEntries
            .map((refuel) => RefuelLogbookEntry(
                  onPressed: () => controller.deleteRefuelEntry(refuel.id),
                  refuelLocation: refuel.position,
                  refuelDate: refuel.creationDateIsoString,
                  mileage: int.parse(refuel.distanceMeters) ~/ 1000,
                  refilledMilliliters: refuel.consumptionMilliliter,
                  pricePerLiterDeziCent: refuel.pricePerLiterCents,
                  totalPriceCents: refuel.totalCostsCent,
                ))
            .toList() ??
        [];
  }

  List<Widget> getCurrentRouteEntries() {
    return controller.vehicles
            .map((element) => element.vehicles)
            .toList()
            .expand((e) => e)
            .toList()
            .firstWhereOrNull(
                (car) => car.id == controller.selectedVehicleId.value)
            ?.routeEntries
            .map((route) => RouteLogbookEntry(
                  onPressed: () => controller.deleteRouteEntry(route.id),
                  routeStart: route.departure,
                  routeDestination: route.destination,
                  routeLengthMeters: int.parse(route.distanceMeters),
                  fuelConsumptionMilliLiters: route.consumptionMilliliter,
                  routeDate: route.creationDateIsoString,
                  driver: route.name,
                  costsCents: route.totalCostsCent,
                ))
            .toList() ??
        [];
  }

  Widget buildVehiclesRow() => SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Obx(
          () => Row(
            children: getVehiclesWidgets(),
          ),
        ),
      );

  List<Widget> getVehiclesWidgets() {
    List<Widget> vehicles = controller.vehicles
        .map((group) {
          var color = Color(
                  (math.Random(group.groupID.hashCode).nextDouble() * 0xFFFFFF)
                      .toInt())
              .withOpacity(1.0);
          return group.vehicles
              .map((vehicle) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: LogbookVehicle(
                      groupName: group.groupID != controller.ownGroupId
                          ? group.name
                          : null,
                      color: color,
                      active: controller.selectedVehicleId.value == vehicle.id,
                      name: vehicle.name,
                      onClick: () {
                        controller.setSelectedVehicle(id: vehicle.id);
                      },
                    ),
                  ))
              .toList();
        })
        .toList()
        .expand((element) => element)
        .toList();
    vehicles.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: LogbookAddButtonVehicle(
        onClick: () {
          createVehicleDialog();
        },
      ),
    ));
    return vehicles;
  }

  Future showAddEntryDialog() {
    return Get.dialog(
      Material(
        child: Padding(
          padding: const EdgeInsets.all(64.0),
          child: Column(
            children: [createRouteEntry(), Divider(), createRefuelEntry()],
          ),
        ),
      ),
    );
  }

  Widget createRefuelEntry() {
    return Column(children: [
      Text("New Refuel entry", style: TextStyle(fontSize: 34)),
      Container(
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: controller.refuelEntryPositionTextController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Position',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.refuelEntryMileageTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Mileage (km)',
                ),
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.refuelEntryConsumptionTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Amount (l)',
                ),
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.refuelEntryCostsTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Costs (€)',
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.refuelEntryLiterCostTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Costs pro Litre (€)',
                ),
              ),
            ),
          ),
        ],
      ),
      Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Create Refuel entry',
                    style: TextStyle(fontSize: 20.0)),
              ),
              onPressed: () {
                controller.createRefuelEntry();
                Get.close(1);
              },
            ),
          ),
        ],
      )
    ]);
  }

  Widget createRouteEntry() {
    return Column(children: [
      Text("New Route entry", style: TextStyle(fontSize: 34)),
      Container(
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: controller.routeEntryDepartureTextController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Departure',
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: controller.routeEntryDestinationTextController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Destination',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.routeEntryDistanceTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Distance (km)',
                ),
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.routeEntryConsumptionTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Consumption (l)',
                ),
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: controller.routeEntryCostsTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Costs (€)',
                ),
              ),
            ),
          ),
        ],
      ),
      Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Create Route entry',
                    style: TextStyle(fontSize: 20.0)),
              ),
              onPressed: () {
                controller.createRouteEntry();
                Get.close(1);
              },
            ),
          ),
        ],
      )
    ]);
  }

  Future showGroupsDialog() {
    return Get.dialog(
      Material(
        child: Padding(
          padding: const EdgeInsets.all(64.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Your overview",
                  style: TextStyle(fontSize: 30),
                ),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Your personal group:",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  SelectableText(controller.ownGroupId ?? ""),
                ],
              ),
              SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Groups you're member in:",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: controller.vehicles
                        .where((e) => e.groupID != controller.ownGroupId)
                        .map((element) {
                      return Row(
                        children: [
                          SelectableText(element.name + "        "),
                          SelectableText(element.groupID),
                          ElevatedButton(
                            child:
                                Text('Leave', style: TextStyle(fontSize: 20.0)),
                            onPressed: () {
                              controller.leaveGroup(groupId: element.groupID);
                              Get.close(1);
                            },
                          ),
                        ],
                      );
                    }).toList(),
                  ),
                ],
              ),
              Divider(),
              TextField(
                controller: controller.joinGroupBrandTextController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Group ID to join',
                ),
              ),
              Row(
                children: [
                  ElevatedButton(
                    child: Text('Join', style: TextStyle(fontSize: 20.0)),
                    onPressed: () {
                      controller.joinGroup();
                      Get.close(1);
                    },
                  ),
                  SizedBox(width: 12),
                ],
              ),
              Expanded(child: Container()),
              ElevatedButton(
                child: Text('Close', style: TextStyle(fontSize: 20.0)),
                onPressed: () {
                  Get.close(1);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future createVehicleDialog() {
    return Get.dialog(Material(
      child: Padding(
        padding: const EdgeInsets.all(64.0),
        child: Column(
          children: [
            Text(
              "Create Veicle",
              style: TextStyle(fontSize: 24),
            ),
            TextField(
              controller: controller.createVehicleNameTextController,
              keyboardType: TextInputType.visiblePassword,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Name',
              ),
            ),
            SizedBox(height: 10),
            TextField(
              controller: controller.createVehicleBrandTextController,
              keyboardType: TextInputType.visiblePassword,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Brand',
              ),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: Text(
                    'Create',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  onPressed: controller.createVehicle,
                ),
                SizedBox(width: 16),
                ElevatedButton(
                  child: Text(
                    'Cancel',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  onPressed: () {
                    Get.close(1);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }
}

abstract class LogbookControllerInterface {
  RxList<LogbookPageModelGroup> get vehicles;
  TextEditingController get createVehicleNameTextController;
  TextEditingController get createVehicleBrandTextController;
  TextEditingController get joinGroupBrandTextController;

  TextEditingController get routeEntryDepartureTextController;
  TextEditingController get routeEntryDestinationTextController;
  TextEditingController get routeEntryDistanceTextController;
  TextEditingController get routeEntryConsumptionTextController;
  TextEditingController get routeEntryCostsTextController;

  String? get ownGroupId;

  TextEditingController get refuelEntryPositionTextController;
  TextEditingController get refuelEntryMileageTextController;
  TextEditingController get refuelEntryConsumptionTextController;
  TextEditingController get refuelEntryCostsTextController;
  TextEditingController get refuelEntryLiterCostTextController;

  bool get isLoggenInLogbook;

  Future<bool> logout();
  Future<void> createVehicle();
  Future<void> fetchVehicles();
  Future<void> generateInvoice();

  void createRouteEntry();
  void createRefuelEntry();

  void setSelectedVehicle({required String id});
  Rx<String?> get selectedVehicleId;

  void leaveGroup({required String groupId});
  void joinGroup();
  void deleteVehicle();

  void deleteRefuelEntry(String id);
  void deleteRouteEntry(String id);
}
