import 'dart:collection';
import 'dart:convert';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:latlong2/latlong.dart';
import 'package:rxdart/rxdart.dart' as rx;
import 'package:kdtree/kdtree.dart';
import 'package:spritradar/home/gas_entry.dart';
import 'package:spritradar/home/gas_type.dart';
import 'package:tuple/tuple.dart';
import 'package:spritradar/home/gas_station_model.dart';

import 'home_page.dart';

extension Points on LatLng {
  Map<String, double> toPoint() {
    return {'lat': this.latitude, 'lon': this.longitude};
  }
}

typedef Latitude = double;
typedef Longitude = double;

abstract class HomeRestService {
  Future<List<GasStationModel>> fetchStations();

  Future<String> fetchPrices({
    required List<GasStationModel> stations,
  });

  bool isLoggedId(); 
}

class HomeController extends GetxController implements HomeControllerInterface {
  final HomeRestService service = Get.find();

  late Box stationBox;
  final Rx<double> zoomLevel = 15.0.obs;

  final Rx<GasStationModel?> _selectedGasStation = Rx(null);
  final Rx<GasType> _userPreferredGasType = GasType.SUPER_E5.obs;
  final Rx<LatLng> _userPosition = LatLng(47.66, 9.17).obs;

  final Rx<LatLngBounds> _mapBounds =
      LatLngBounds(LatLng(47.66, 9.17), LatLng(47.66, 9.17)).obs;

  static final Distance distance = new Distance();
  static final stationDistance = (a, b) {
    return distance.as(LengthUnit.Meter, LatLng(a['lat'], a['lon']),
        LatLng(b['lat'], b['lon']));
  };
  KDTree _stationsTree = KDTree([], stationDistance, []);
  Map<Latitude, Map<Longitude, GasStationModel>> _latLonStationsMap = Map();
  final RxList<GasStationModel> _gasStationsInRadius = RxList();

  bool isBoxOutdated(String stationTimeStamp) => DateTime.now()
      .isAfter(DateTime.parse(stationTimeStamp).add(Duration(days: 1)));

  void updateStationBox(List<GasStationModel> stations) {
    print('Setting in box: ${stations.length} stations');
    final timestamp = DateTime.now().toIso8601String();
    stationBox.put("stations", stations);
    stationBox.put("timestamp", timestamp);
  }

  @override
  void onInit() async {
    stationBox = await Hive.openBox('stationBox');

    super.onInit();
    _gasStationsInRadius.bindStream(_mapBounds.stream
        .skip(1)
        .throttleTime(Duration(milliseconds: 100))
        .asyncMap((bounds) => stationsInRadius(bounds)));
    final stationTuple = await getStationTreeAndMap();
    _stationsTree = stationTuple.item1;
    _latLonStationsMap = stationTuple.item2;
  }

  @override
  RxList<GasStationModel> get gasStationsInRadius => _gasStationsInRadius;

  @override
  Rx<LatLng> get userPosition => _userPosition;

  @override
  Rx<GasStationModel?> get selectedGasStation => _selectedGasStation;

  @override
  Rx<GasType> get userPreferedGasType => _userPreferredGasType;

  @override
  void setSelectedGasStation({required GasStationModel gasStation}) {
    if (_selectedGasStation.value == gasStation) {
      _selectedGasStation.value = null;
    } else {
      _selectedGasStation.value = gasStation;
    }
  }

  @override
  GasStationModel? cheapestGasStationOf(List<GasStationModel> gasStations) {
    return gasStations
        .where((station) =>
            station.gasEntryByType(type: userPreferedGasType.value) != null)
        .reduce((current, cheapest) {
      return cheapest
                  .gasEntryByType(type: userPreferedGasType.value)!
                  .gasPriceCents <
              current
                  .gasEntryByType(type: userPreferedGasType.value)!
                  .gasPriceCents
          ? cheapest
          : current;
    });
  }

  @override
  bool isCheapest({required GasStationModel station}) =>
      station == cheapestGasStationOf(_gasStationsInRadius);

  Future<Tuple2<KDTree, Map<Latitude, Map<Longitude, GasStationModel>>>>
      getStationTreeAndMap() async {
    final stations = await getAllStations();
    final stationPoints = stations.map((e) => e.position.toPoint()).toList();
    Map<Latitude, Map<Longitude, GasStationModel>> stationMap = HashMap();
    stations.forEach((s) {
      stationMap.update(
        s.position.latitude,
        (lonMap) {
          lonMap[s.position.longitude] = s;
          return lonMap;
        },
        ifAbsent: () => {s.position.longitude: s},
      );
    });
    final kdTree = KDTree(stationPoints, stationDistance, ['lat', 'lon']);
    return Tuple2(kdTree, stationMap);
  }

  Future<List<GasStationModel>> getAllStations() async {
    List<GasStationModel> stations;
    final stationTimeStamp = stationBox.get("timestamp");
    print('Timestamp in hive: $stationTimeStamp');
    if (stationTimeStamp == null || isBoxOutdated(stationTimeStamp)) {
      print('Requesting stations from server...');
      stations = await service.fetchStations();
      updateStationBox(stations);
    } else {
      print('Using stations from hive');
      List<dynamic> dynamicStations = stationBox.get("stations");
      stations = dynamicStations.map((e) => e as GasStationModel).toList();
    }
    print("Got ${stations.length} stations");
    return stations;
  }

  @override
  void setPosition(MapPosition position, bool hasGesture) {
    _mapBounds.value = position.bounds!;
    // zoomLevel.value = position.zoom!;
    // print('Setting mapBounds to center ${_mapBounds.value.center}');
  }

  Future<List<GasStationModel>> stationsInRadius(LatLngBounds bounds) async {
    final maxDistance = distance(bounds.center, bounds.northWest).ceil();
    final stationPoints = _stationsTree
        .nearest(bounds.center.toPoint(), 50000, maxDistance)
        .map((e) => e[0] as Map<String, double>)
        .toList();
    final stations = getFromLatLonMap(stationPoints);

    Map<String, List<GasEntry>> gasEntries =
        await getGasEntriesForStations(stations);
    print(
        'Found ${stations.length} stations for ${bounds.center}. max distance: ${maxDistance}m');
    final stationsUpdated = stations
        .map((station) => station.copyWith(gasEntries: gasEntries[station.id]))
        .where((station) => station.gasEntries.isNotEmpty)
        .toList();
    stationsUpdated.forEach((station) {
      _latLonStationsMap[station.position.latitude]![
          station.position.longitude] = station;
    });
    return getFromLatLonMap(stationPoints)
        .where((station) => station.gasEntries.isNotEmpty)
        .toList();
  }

  List<GasStationModel> getFromLatLonMap(List<Map<String, double>> points) {
    var x = points
        .map((point) => _latLonStationsMap[point['lat']]![point['lon']]!)
        .toList();
    return x;
  }

  Future<Map<String, List<GasEntry>>> getGasEntriesForStations(
      List<GasStationModel> stations,
      {int chunkSize = 50}) async {
    Map<String, List<GasEntry>> gasEntries = Map();
    final chunkSize = 50;
    for (var i = 0; i < stations.length; i += chunkSize) {
      final subList = stations.sublist(
          i, i + chunkSize > stations.length ? stations.length : i + chunkSize);
      final slice = (json.decode(await getGasEntriesResponse(subList))
              as Map<String, dynamic>)
          .map((key, value) =>
              MapEntry(key, GasStationModel.parseGasEntries(value)));
      gasEntries.addAll(slice);
    }
    return gasEntries;
  }

  Future<String> getGasEntriesResponse(List<GasStationModel> stations) async {
    return service.fetchPrices(stations: stations);
  }

  @override
  bool get isLoggenIn => service.isLoggedId();
}
