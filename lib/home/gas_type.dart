import 'package:hive/hive.dart';

part 'gas_type.g.dart';

@HiveType(typeId: 4)
enum GasType {
  @HiveField(0)
  SUPER_E5,
  @HiveField(1)
  SUPER_E10,
  @HiveField(2)
  DIESEL,
}

extension GasTypeName on GasType {
  String get gasName {
    switch (this) {
      case GasType.SUPER_E5:
        return 'Super E5';
      case GasType.SUPER_E10:
        return 'Super E10';
      case GasType.DIESEL:
        return 'Diesel';
    }
  }
  String get mapKey {
    switch (this) {
      case GasType.SUPER_E5:
        return 'superE5DeciCents';
      case GasType.SUPER_E10:
        return 'superE10DeciCents';
      case GasType.DIESEL:
        return 'dieselDeciCents';
    }
  }
}
