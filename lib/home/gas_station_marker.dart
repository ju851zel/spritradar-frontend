import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:spritradar/home/gas_station_price.dart';
import 'package:spritradar/home/gas_station_model.dart';
import 'package:spritradar/home/gas_type.dart';

class AggregatedGasStationChip extends StatelessWidget {
  final GasStationModel gasStation;
  final GasType userPreferedGasType;
  final int aggregatedNumber;
  final VoidCallback onTap;
  final bool showBorder;

  const AggregatedGasStationChip({
    required this.gasStation,
    required this.userPreferedGasType,
    required this.aggregatedNumber,
    required this.onTap,
    this.showBorder = false,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.all(4),
          child: SingleGasStationChip(
            onTap: onTap,
            gasStation: gasStation,
            userPreferedGasType: userPreferedGasType,
          ),
        ),
        buildAggregatedNumber()
      ],
    );
  }

  Align buildAggregatedNumber() => Align(
        alignment: Alignment.topRight,
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              shape: BoxShape.circle,
            ),
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: Text(aggregatedNumber.toString()),
            )),
      );
}

class SingleGasStationChip extends StatelessWidget {
  final GasStationModel gasStation;
  final GasType userPreferedGasType;
  final bool showBorder;
  final VoidCallback onTap;

  const SingleGasStationChip({
    required this.gasStation,
    required this.onTap,
    required this.userPreferedGasType,
    this.showBorder = false,
  });

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: onTap,
        child: Bubble(
          nip: BubbleNip.leftCenter,
          color: Colors.white,
          borderColor: showBorder ? Colors.blueAccent : null,
          borderWidth: 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(flex: 1, child: _gasStationName()),
              Expanded(flex: 2, child: _gasStationPrice()),
            ],
          ),
        ),
      );

  Widget _gasStationName() => FittedBox(
        fit: BoxFit.contain,
        child: Text(
          gasStation.name,
          style: TextStyle(
            color: Colors.red,
          ),
        ),
      );

  Widget _gasStationPrice() => FittedBox(
        fit: BoxFit.contain,
        child: GasStationPrice(
          gasStationPriceCents: gasStation.gasEntryByType(type: userPreferedGasType)!.gasPriceCents,
          gasStationPriceSuffix: gasStation
              .gasEntryByType(type: userPreferedGasType)!
              .gasPriceSuffix,
        ),
      );
}
