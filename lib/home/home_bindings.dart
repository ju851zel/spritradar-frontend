
import 'package:get/get.dart';
import 'package:spritradar/home/home_controller.dart';

import 'home_page.dart';

class HomeBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<HomeControllerInterface>(HomeController());
  }
}
