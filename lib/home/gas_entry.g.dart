// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gas_entry.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class GasEntryAdapter extends TypeAdapter<GasEntry> {
  @override
  final int typeId = 3;

  @override
  GasEntry read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return GasEntry(
      gasName: fields[0] as String,
      type: fields[1] as GasType,
      gasPriceCents: fields[2] as int,
      gasPriceSuffix: fields[3] as int,
      timestamp: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, GasEntry obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.gasName)
      ..writeByte(1)
      ..write(obj.type)
      ..writeByte(2)
      ..write(obj.gasPriceCents)
      ..writeByte(3)
      ..write(obj.gasPriceSuffix)
      ..writeByte(4)
      ..write(obj.timestamp);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GasEntryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
