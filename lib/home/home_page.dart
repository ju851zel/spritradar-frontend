import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:get/get.dart';
import 'package:latlong2/latlong.dart';
import 'package:spritradar/commons/scaffold.dart';
import 'package:spritradar/home/gas_station_marker.dart';
import 'package:spritradar/home/gas_station_model.dart';
import 'package:spritradar/home/gas_type.dart';

import 'gas_station_details.dart';

class HomePage extends GetView<HomeControllerInterface> {
  final double _width = 80.0;
  final double _height = 60.0;

  @override
  Widget build(BuildContext context) => Obx(
        () => MyScaffold(
          onTab: (index) {
            if (index == 0) {
              if (controller.isLoggenIn) {
                Get.toNamed('/logbook');
              } else {
                Get.toNamed('/login');
              }
            }
            if (index == 2) {
              if (controller.isLoggenIn) {
                Get.offAndToNamed('/stats');
              } else {
                Get.toNamed('/login');
              }
            }
          },
          pageIndex: 1,
          child: Stack(
            children: [
              _buildMap(),
              _buildGasStationDetailsView(
                selectedGasStation: controller.selectedGasStation.value,
              ),
            ],
          ),
        ),
      );

  Widget _buildGasStationDetailsView({
    GasStationModel? selectedGasStation,
  }) =>
      selectedGasStation == null
          ? Container()
          : Column(
              children: [
                Expanded(flex: 2, child: Container()),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      color: Colors.white,
                      width: double.infinity,
                      child: GasStationDetails(
                        details: selectedGasStation,
                      ),
                    ),
                  ),
                ),
              ],
            );

  Widget _buildMap() => FlutterMap(
        options: MapOptions(
          center: controller.userPosition.value,
          zoom: controller.zoomLevel.value,
          minZoom: 8.0,
          plugins: [
            MarkerClusterPlugin(),
          ],
          onPositionChanged: (a, b) => controller.setPosition(a, b),
        ),
        layers: [
          TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c'],
            attributionBuilder: (_) => Text("© OpenStreetMap contributors"),
          ),
          MarkerClusterLayerOptions(
            maxClusterRadius: 120,
            size: Size(_width, _height),
            markers: controller.zoomLevel.value <= 11
                ? []
                : controller.gasStationsInRadius
                    .where((station) =>
                        station.gasEntryByType(
                            type: controller.userPreferedGasType.value) !=
                        null)
                    .map(
                      (station) => _buildMarker(
                        gasStation: station,
                        userPreferedGasType:
                            controller.userPreferedGasType.value,
                        showBorder: controller.isCheapest(station: station),
                      ),
                    )
                    .toList(),
            polygonOptions: PolygonOptions(
              borderColor: Colors.blueAccent,
              color: Colors.black12,
              borderStrokeWidth: 3,
            ),
            builder: (context, markers) {
              var gasStation = controller.cheapestGasStationOf(
                _mapMarkersToGasStation(
                  markers: markers,
                  context: context,
                ),
              );
              if (gasStation == null ||
                  gasStation.gasEntryByType(
                          type: controller.userPreferedGasType.value) ==
                      null) return Container();
              return AggregatedGasStationChip(
                aggregatedNumber: markers.length,
                userPreferedGasType: controller.userPreferedGasType.value,
                gasStation: gasStation,
                onTap: () => controller.setSelectedGasStation(
                  gasStation: gasStation,
                ),
              );
            },
          ),
        ],
      );

  List<GasStationModel> _mapMarkersToGasStation({
    required List<Marker> markers,
    required BuildContext context,
  }) =>
      markers
          .map((e) => (e.builder(context) as SingleGasStationChip).gasStation)
          .toList();

  Marker _buildMarker({
    required GasStationModel gasStation,
    required GasType userPreferedGasType,
    required bool showBorder,
  }) =>
      Marker(
        width: _width,
        height: _height,
        point: gasStation.position,
        builder: (ctx) => SingleGasStationChip(
          onTap: () => controller.setSelectedGasStation(gasStation: gasStation),
          gasStation: gasStation,
          userPreferedGasType: userPreferedGasType,
          showBorder: showBorder,
        ),
      );
}

abstract class HomeControllerInterface {
  RxList<GasStationModel> get gasStationsInRadius;

  Rx<GasType> get userPreferedGasType;

  Rx<GasStationModel?> get selectedGasStation;

  Rx<LatLng> get userPosition;

  bool get isLoggenIn;

  Rx<double> get zoomLevel;

  void setSelectedGasStation({required GasStationModel gasStation});

  GasStationModel? cheapestGasStationOf(List<GasStationModel> gasStations);

  bool isCheapest({required GasStationModel station});

  void setPosition(MapPosition position, bool hasGesture);
}
