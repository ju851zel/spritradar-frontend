import 'package:flutter/material.dart';
import 'package:spritradar/home/gas_station_model.dart';
import 'package:spritradar/home/gas_station_price.dart';
import 'package:get/get.dart';

class GasStationDetails extends StatelessWidget {
  final GasStationModel details;

  const GasStationDetails({
    required this.details,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
        children: [
          _gasStationInfos(),
          Divider(),
          SelectableText(details.id, style: TextStyle(fontSize: 12)),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: gasEntries(),
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> gasEntries() => details.gasEntries
      .map(
        (gasEntry) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(gasEntry.gasName),
            GasStationPrice(
              gasStationPriceCents: gasEntry.gasPriceCents,
              gasStationPriceSuffix: gasEntry.gasPriceSuffix,
            ),
          ],
        ),
      )
      .toList();

  Widget _gasStationInfos() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(flex: 1, child: Icon(Icons.access_alarm)),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(details.name, style: TextStyle(fontSize: 16)),
                  Text(details.street + ' ' + details.houseNr,
                      style: TextStyle(fontSize: 12)),
                  Text(details.zipCode + " " + details.city,
                      style: TextStyle(fontSize: 12)),
                ],
              ),
            ),
            Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("openinghours_title".tr),
                    Text(details.openingHours),
                    Text(
                        '${"timestamp_title".tr} ${details.gasEntries.first.timestamp}'),
                  ],
                )),
          ],
        ),
      );
}
