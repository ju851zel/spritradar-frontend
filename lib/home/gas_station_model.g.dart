// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gas_station_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class GasStationModelAdapter extends TypeAdapter<GasStationModel> {
  @override
  final int typeId = 1;

  @override
  GasStationModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return GasStationModel(
      id: fields[0] as String,
      name: fields[1] as String,
      street: fields[2] as String,
      houseNr: fields[3] as String,
      zipCode: fields[4] as String,
      city: fields[5] as String,
      openingHours: fields[6] as String,
      position: fields[7] as LatLng,
      gasEntries: (fields[8] as List).cast<GasEntry>(),
    );
  }

  @override
  void write(BinaryWriter writer, GasStationModel obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.street)
      ..writeByte(3)
      ..write(obj.houseNr)
      ..writeByte(4)
      ..write(obj.zipCode)
      ..writeByte(5)
      ..write(obj.city)
      ..writeByte(6)
      ..write(obj.openingHours)
      ..writeByte(7)
      ..write(obj.position)
      ..writeByte(8)
      ..write(obj.gasEntries);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GasStationModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
