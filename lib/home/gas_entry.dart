import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:collection/collection.dart';
import 'package:spritradar/home/gas_type.dart';


part 'gas_entry.g.dart';

@HiveType(typeId: 3)
class GasEntry {
  @HiveField(0)
  final String gasName;
  @HiveField(1)
  final GasType type;
  @HiveField(2)
  final int gasPriceCents;
  @HiveField(3)
  final int gasPriceSuffix;
  @HiveField(4)
  final String timestamp;

  static final DateFormat dateFormat = new DateFormat(DateFormat.HOUR24_MINUTE);

//<editor-fold desc="Data Methods">

  const GasEntry({
    required this.gasName,
    required this.type,
    required this.gasPriceCents,
    required this.gasPriceSuffix,
    required this.timestamp,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          (other is GasEntry &&
              runtimeType == other.runtimeType &&
              gasName == other.gasName &&
              type == other.type &&
              gasPriceCents == other.gasPriceCents &&
              gasPriceSuffix == other.gasPriceSuffix &&
              timestamp == other.timestamp);

  @override
  int get hashCode =>
      gasName.hashCode ^
      type.hashCode ^
      gasPriceCents.hashCode ^
      gasPriceSuffix.hashCode ^
      timestamp.hashCode;

  @override
  String toString() {
    return 'GasEntry{' +
        ' gasName: $gasName,' +
        ' type: $type,' +
        ' gasPriceCents: $gasPriceCents,' +
        ' gasPriceSuffix: $gasPriceSuffix,' +
        ' timestamp: $timestamp,' +
        '}';
  }

  GasEntry copyWith({
    String? gasName,
    GasType? type,
    int? gasPriceCents,
    int? gasPriceSuffix,
    String? timestamp,
  }) {
    return GasEntry(
        gasName: gasName ?? this.gasName,
        type: type ?? this.type,
        gasPriceCents: gasPriceCents ?? this.gasPriceCents,
        gasPriceSuffix: gasPriceSuffix ?? this.gasPriceSuffix,
        timestamp: timestamp ?? this.timestamp
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'gasName': this.gasName,
      'type': this.type,
      'gasPriceCents': this.gasPriceCents,
      'gasPriceSuffix': this.gasPriceSuffix,
      'timestamp': this.timestamp,
    };
  }

  factory GasEntry.fromMap(Map<String, dynamic> map) {
    return GasEntry(
      gasName: map['gasName'] as String,
      type: map['type'] as GasType,
      gasPriceCents: map['gasPriceCents'] as int,
      gasPriceSuffix: map['gasPriceSuffix'] as int,
      timestamp: dateFormat.format(DateTime.parse(map['timestampIsoString']).toLocal()),
    );
  }

//</editor-fold>
}
