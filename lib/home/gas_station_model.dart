import 'package:hive/hive.dart';
import 'package:latlong2/latlong.dart';
import 'package:collection/collection.dart';
import 'package:spritradar/home/gas_entry.dart';
import 'package:spritradar/home/gas_type.dart';

part 'gas_station_model.g.dart';

@HiveType(typeId: 1)
class GasStationModel {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String name;
  @HiveField(2)
  final String street;
  @HiveField(3)
  final String houseNr;
  @HiveField(4)
  final String zipCode;
  @HiveField(5)
  final String city;
  @HiveField(6)
  final String openingHours;
  @HiveField(7)
  final LatLng position;
  @HiveField(8)
  final List<GasEntry> gasEntries;

  GasEntry? gasEntryByType({required GasType type}) {
    return gasEntries.firstWhereOrNull((element) => element.type == type);
  }

//<editor-fold desc="Data Methods">

  const GasStationModel({
    required this.id,
    required this.name,
    required this.street,
    required this.houseNr,
    required this.zipCode,
    required this.city,
    required this.openingHours,
    required this.position,
    required this.gasEntries,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is GasStationModel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          street == other.street &&
          houseNr == other.houseNr &&
          zipCode == other.zipCode &&
          city == other.city &&
          openingHours == other.openingHours &&
          position == other.position &&
          gasEntries == other.gasEntries);

  @override
  int get hashCode =>
      id.hashCode ^
      name.hashCode ^
      street.hashCode ^
      houseNr.hashCode ^
      zipCode.hashCode ^
      city.hashCode ^
      openingHours.hashCode ^
      position.hashCode ^
      gasEntries.hashCode;

  @override
  String toString() {
    return 'GasStationModel{' +
        ' id: $id,' +
        ' name: $name,' +
        ' street: $street,' +
        ' houseNr: $houseNr,' +
        ' zipCode: $zipCode,' +
        ' city: $city,' +
        ' openingHours: $openingHours,' +
        ' position: $position,' +
        ' gasEntries: $gasEntries,' +
        '}';
  }

  GasStationModel copyWith({
    String? id,
    String? name,
    String? street,
    String? houseNr,
    String? zipCode,
    String? city,
    String? openingHours,
    LatLng? position,
    List<GasEntry>? gasEntries,
  }) {
    return GasStationModel(
      id: id ?? this.id,
      name: name ?? this.name,
      street: street ?? this.street,
      houseNr: houseNr ?? this.houseNr,
      zipCode: zipCode ?? this.zipCode,
      city: city ?? this.city,
      openingHours: openingHours ?? this.openingHours,
      position: position ?? this.position,
      gasEntries: gasEntries ?? this.gasEntries,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
      'street': this.street,
      'houseNr': this.houseNr,
      'zip': this.zipCode,
      'city': this.city,
      'openingHours': this.openingHours,
      'position': this.position,
      'gasEntries': this.gasEntries,
    };
  }

  factory GasStationModel.fromMap(Map<String, dynamic> map) {
    return GasStationModel(
      id: map['id'] as String,
      name: map['name'] as String,
      street: map['street'] as String,
      houseNr: map['houseNr'] as String,
      zipCode: map['zip'] as String,
      city: map['city'] as String,
      openingHours: "",
      position: LatLng(map['position']['lat'], map['position']['lon']),
      gasEntries: parseGasEntries(map['currentPrice']),
    );
  }

  static List<GasEntry> parseGasEntries(Map<String, dynamic>? map) {
    List<GasEntry> gasEntries = [];
    if (map == null) {
      return gasEntries;
    }
    GasType.values.forEach((e) => {
          if (map[e.mapKey] != null)
            {
              gasEntries.add(GasEntry(
                  gasName: e.gasName,
                  type: e,
                  gasPriceCents: (map[e.mapKey] / 10).floor(),
                  gasPriceSuffix: map[e.mapKey] % 10,
                  timestamp: GasEntry.dateFormat.format(
                      DateTime.parse(map['timestampIsoString']).toLocal())))
            }
        });
    return gasEntries;
  }

//</editor-fold>
}

class LatLngAdapter extends TypeAdapter<LatLng> {
  @override
  final int typeId = 2;

  @override
  LatLng read(BinaryReader reader) {
    var lat = reader.readDouble();
    var lon = reader.readDouble();
    return LatLng(lat, lon);
  }

  @override
  void write(BinaryWriter writer, LatLng obj) {
    writer.writeDouble(obj.latitude);
    writer.writeDouble(obj.longitude);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is LatLngAdapter &&
              runtimeType == other.runtimeType &&
              typeId == other.typeId;
}
