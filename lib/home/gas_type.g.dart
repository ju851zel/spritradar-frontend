// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gas_type.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class GasTypeAdapter extends TypeAdapter<GasType> {
  @override
  final int typeId = 4;

  @override
  GasType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return GasType.SUPER_E5;
      case 1:
        return GasType.SUPER_E10;
      case 2:
        return GasType.DIESEL;
      default:
        return GasType.SUPER_E5;
    }
  }

  @override
  void write(BinaryWriter writer, GasType obj) {
    switch (obj) {
      case GasType.SUPER_E5:
        writer.writeByte(0);
        break;
      case GasType.SUPER_E10:
        writer.writeByte(1);
        break;
      case GasType.DIESEL:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GasTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
