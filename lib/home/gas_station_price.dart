import 'package:flutter/material.dart';

class GasStationPrice extends StatelessWidget {
  final int gasStationPriceCents;
  final int gasStationPriceSuffix;
  final String? unit;

  const GasStationPrice({
    required this.gasStationPriceCents,
    required this.gasStationPriceSuffix,
    this.unit,
  });

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(children: [
        TextSpan(
          text: (gasStationPriceCents / 100).toStringAsFixed(2),
          style: TextStyle(color: Colors.black),
        ),
        WidgetSpan(
          child: Transform.translate(
            offset: const Offset(2, -4),
            child: Text(
              gasStationPriceSuffix.toString(),
              textScaleFactor: 0.7,
            ),
          ),
        ),
        TextSpan(
          text: unit,
          style: TextStyle(color: Colors.black),
        ),
      ]),
    );
  }
}
