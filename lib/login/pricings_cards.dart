import 'package:flutter/material.dart';
import 'package:pricing_cards/pricing_cards.dart';

class PricingCardsWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          PricingCards(
            pricingCards: [
              PricingCard(
                title: 'Base',
                price: 'Free',
                subPriceText: 'forever',
                billedText: '\n        Spritradar        \n',
                onPress: () {
                },
                cardColor: Colors.green,
                priceStyle: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
                titleStyle: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
                billedTextStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                subPriceStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                cardBorder: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.red, width: 4.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
              PricingCard(
                title: 'Premium',
                price: '1.00€',
                subPriceText: '\/mo',
                billedText: '- All of Base\n- Vehicles\n- Stats',
                onPress: () {
                  // make your business
                },
                cardColor: Colors.red.shade400,
                priceStyle: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
                titleStyle: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
                billedTextStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                subPriceStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                cardBorder: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.red, width: 4.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
              PricingCard(
                title: 'Enterpise',
                price: '20,00€',
                subPriceText: '\/mo + 50ct/User',
                billedText: '- All of Premium\n- Dedicated Servers\n- Billing',
                onPress: () {
                  // make your business
                },
                cardColor: Colors.blue,
                priceStyle: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
                titleStyle: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
                billedTextStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                subPriceStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
