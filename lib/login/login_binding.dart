import 'package:get/get.dart';
import 'package:spritradar/login/login_controller.dart';
import 'package:spritradar/login/login_page.dart';
import 'package:spritradar/services/rest_service.dart';


class LoginBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<LoginControllerInterface>(LoginController());
  }
}
