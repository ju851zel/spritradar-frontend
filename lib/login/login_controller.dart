import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:spritradar/login/login_page.dart';

class LoginController extends GetxController
    implements LoginControllerInterface {
  final LoginRestService service = Get.find();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Future<void> onInit() async {
    super.onInit();
    _nameController.text = "Julian";
    _emailController.text = "julian@zellner.cc";
    _passwordController.text = "testtest";
  }

  @override
  TextEditingController get nameController => _nameController;

  @override
  TextEditingController get emailController => _emailController;

  @override
  TextEditingController get passwordController => _passwordController;

  @override
  void login() async {
    bool success = await service.login(
      email: _emailController.text,
      password: _passwordController.text,
    );
    if (success) {
      Get.offAndToNamed('/logbook');
    } else {
      Fluttertoast.showToast(
          msg: "Could not login :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  @override
  void register({required bool isEnterprise}) async {
    bool success = await service.register(
      name: _nameController.text,
      email: _emailController.text,
      password: _passwordController.text,
      isEnterprise: isEnterprise,
    );
    if (success) {
      Get.offAndToNamed('/logbook');
    } else {
      Fluttertoast.showToast(
          msg: "Could not register and login :(",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}

abstract class LoginRestService {
  Future<bool> register({
    required String name,
    required String email,
    required String password,
    required bool isEnterprise,
  });

  Future<bool> login({required String email, required String password});
}
