import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:spritradar/commons/scaffold.dart';
import 'package:spritradar/login/pricings_cards.dart';

class LoginPage extends GetView<LoginControllerInterface> {
  @override
  Widget build(BuildContext context) => MyScaffold(
        pageIndex: 0,
        onTab: (index) {
          if (index == 1) {
            Get.offAndToNamed('/home');
          }
          if (index == 2) {
            Get.offAndToNamed('/stats');
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("Welcome", style: TextStyle(fontSize: 60)),
              SizedBox(height: 50),
              PricingCardsWidget(),
              SizedBox(height: 30),
              TextField(
                controller: controller.nameController,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Name',
                ),
              ),
              SizedBox(height: 10),
              TextField(
                controller: controller.emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Email',
                ),
              ),
              SizedBox(height: 10),
              TextField(
                controller: controller.passwordController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  Expanded(child: Container()),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          child: ElevatedButton(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Login',
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ),
                            onPressed: controller.login,
                          ),
                        ),
                        SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          child: ElevatedButton(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Register',
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ),
                            onPressed: () =>
                                controller.register(isEnterprise: false),
                          ),
                        ),
                        SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          child: ElevatedButton(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Register Enterprise',
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ),
                            onPressed: () =>
                                controller.register(isEnterprise: true),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(child: Container())
                ],
              ),
            ],
          ),
        ),
      );
}

abstract class LoginControllerInterface {
  TextEditingController get nameController;
  TextEditingController get emailController;
  TextEditingController get passwordController;
  void login();
  void register({required bool isEnterprise});
}
