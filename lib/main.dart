import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:spritradar/commons/constants.dart';
import 'package:spritradar/home/gas_entry.dart';
import 'package:spritradar/home/gas_type.dart';
import 'package:spritradar/home/gas_station_model.dart';
import 'package:spritradar/login/login_page.dart';
import 'package:spritradar/services/rest_service.dart';
import 'package:spritradar/stats/stats_bindings.dart';
import 'package:spritradar/stats/stats_controller.dart';
import 'package:spritradar/stats/stats_page.dart';

import 'commons/translations.dart';
import 'home/home_bindings.dart';
import 'home/home_controller.dart';
import 'home/home_page.dart';
import 'logbook/logbook_bindings.dart';
import 'logbook/logbook_controller.dart';
import 'logbook/logbook_page.dart';
import 'login/login_binding.dart';
import 'login/login_controller.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(GasEntryAdapter());
  Hive.registerAdapter(GasStationModelAdapter());
  Hive.registerAdapter(LatLngAdapter());
  Hive.registerAdapter(GasTypeAdapter());

  var restService = RestService();
  Get.put<LoginRestService>(restService);
  Get.put<HomeRestService>(restService);
  Get.put<LogbookRestService>(restService);
  Get.put<StatsRestService>(restService);

  runApp(
    MyApp(
      translations: MyTranslations(
        keys: await MyTranslations.parseJsonFromAssets(
          path: Constants.TranslationPath,
        ),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  final Translations _translations;

  MyApp({required Translations translations}) : _translations = translations;

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Spritradar',
      initialRoute: '/home',
      translations: _translations,
      locale: Get.deviceLocale,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      getPages: [
        GetPage(
          name: "/home",
          page: () => HomePage(),
          binding: HomeBindings(),
        ),
        GetPage(
          name: "/login",
          page: () => LoginPage(),
          binding: LoginBindings(),
        ),
        GetPage(
          name: "/logbook",
          page: () => LogbookPage(),
          binding: LogbookBindings(),
        ),
        GetPage(
          name: "/stats",
          page: () => StatsPage(),
          binding: StatsBindings(),
        ),
      ],
    );
  }
}
