import 'package:flutter/cupertino.dart';

class AppTextStyles {
  static TextStyle heading = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );
}
