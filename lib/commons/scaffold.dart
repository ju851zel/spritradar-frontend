import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyScaffold extends StatelessWidget {
  final Widget child;
  final int pageIndex;
  final void Function(int) onTab;

  MyScaffold({
    required this.child,
    required this.pageIndex,
    required this.onTab,
  });

  @override
  Widget build(BuildContext context) => Scaffold(
        body: child,
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTab,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'logbook_title'.tr,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.business),
              label: 'spritradar_title'.tr,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.school),
              label: 'statistics_title'.tr,
            ),
          ],
          currentIndex: pageIndex,
          selectedItemColor: Colors.amber[800],
        ),
      );
}
