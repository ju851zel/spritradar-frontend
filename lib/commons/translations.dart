
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MyTranslations extends Translations {
  final Map<String, Map<String, String>> _keys;

  MyTranslations({required keys}) : _keys = keys;

  static Future<Map<String, Map<String, String>>> parseJsonFromAssets({
    required String path,
  }) async {
    Map<String, Map<String, String>> keys = <String, Map<String, String>>{};
    String jsonString = await rootBundle.loadString(path);
    Map<String, dynamic> json = jsonDecode(jsonString) as Map<String, dynamic>;
    json.entries.forEach((jsonEntry) {
      Map<String, dynamic> entryValueMap =
          jsonEntry.value as Map<String, dynamic>;
      keys[jsonEntry.key] = <String, String>{};
      entryValueMap.entries.forEach((localicationEntry) {
        keys[jsonEntry.key]?[localicationEntry.key] =
            localicationEntry.value.toString();
      });
    });
    return keys;
  }

  @override
  Map<String, Map<String, String>> get keys => _keys;
}
