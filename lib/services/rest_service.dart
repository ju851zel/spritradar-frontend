import 'dart:math';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:spritradar/home/gas_station_model.dart';
import 'package:spritradar/home/home_controller.dart';
import 'dart:convert';
import 'package:spritradar/logbook/logbook_controller.dart';
import 'package:spritradar/login/login_controller.dart';
import 'package:spritradar/stats/stats_controller.dart';
import 'package:spritradar/stats/stats_model.dart';
import 'package:tuple/tuple.dart';

class RestService extends GetxService
    implements
        LogbookRestService,
        StatsRestService,
        LoginRestService,
        HomeRestService {
  // final String _backendUrl = "http://34.120.66.223/api";
  // final String _flottenBackendUrl = "http://34.120.66.223/flotten";
  // final String _statsBackendUrl = "http://34.120.66.223/async";

  String _backendUrl() {
    if (groupId == null) {
      return "https://xyz.zellner.cc/base/api";
    }
    if (_isEnterprise != null) {
      return "https://xyz.zellner.cc/$_isEnterprise/api";
    }
    return "https://xyz.zellner.cc/premium/api";
  }

  String _flottenBackendUrl() {
    if (groupId == null) {
      print("FLOTTEN bASIS");
      return "https://xyz.zellner.cc/base/flotten";
    }
    if (_isEnterprise != null) {
      print("FLOTTEN ENtERPRISE");
      return "https://xyz.zellner.cc/$_isEnterprise/flotten";
    }
    print("FLOTTEN PREMIUM");
    return "https://xyz.zellner.cc/premium/flotten";
  }

  String _statsBackendUrl() {
    if (groupId == null) {
      return "https://xyz.zellner.cc/base/async";
    }
    if (_isEnterprise != null) {
      return "https://xyz.zellner.cc/$_isEnterprise/async";
    }
    return "https://xyz.zellner.cc/premium/async";
  }

  final String apiKey = "AIzaSyDG55DRPL5WsSy4rk4vVllnWb815RzGaI4";
  String? _groupId;
  String? idToken;
  String? refreshToken;
  DateTime? expiresIn;
  String? _isEnterprise;

  @override
  Future<List<LogbookRestServiceGroups>> fetchVehicles() async {
    var groupsImIn = await fetchAllGroupsImIn();
    return await Future.wait(groupsImIn.map((groupIdName) async {
      List<LogbookRestServiceModelVehicle> vehicles =
          (await callRestApi<LogbookRestServiceModelVehicle>(
        url: Uri.parse(
            '${_flottenBackendUrl()}/groups/${groupIdName.item2}/vehicles'),
        token: idToken!,
        mapper: (json) => LogbookRestServiceModelVehicle.fromMap(json),
      ));
      return LogbookRestServiceGroups(
          groupId: groupIdName.item2,
          name: groupIdName.item1,
          vehicles: vehicles);
    }));
  }

  Future<List<Tuple2<String, String>>> fetchAllGroupsImIn() async {
    final response = await http
        .get(Uri.parse('${_flottenBackendUrl()}/groups/$groupId'), headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      return (json.decode(response.body) as List<dynamic>).map((json) {
        return Tuple2(json['name'] as String, json['id'] as String);
      }).toList();
    }
    print(response.body);
    return [];
  }

  @override
  Future<bool> createVehicle({
    required String name,
    required String brand,
  }) async {
    await refreshLogin();
    final response = await http.post(
        Uri.parse("${_flottenBackendUrl()}/groups/$_groupId/vehicles"),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer $idToken",
        },
        body: jsonEncode({
          "id": randomNumer(),
          "name": name,
          "brand": brand,
        }));
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  String randomNumer() {
    var res = "";
    for (var i = 0; i < 16; i++) {
      res += Random.secure().nextInt(10).toString();
    }
    return res;
  }

  @override
  Future<List<LogbookRestServiceModelRefuelEntry>> fetchRefuelEntries({
    required String groupId,
    required String vehicleId,
  }) async {
    var mapper = (json) => LogbookRestServiceModelRefuelEntry.fromMap(json);
    var url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupId/vehicles/$vehicleId/refuels');
    final response = await http.get(url, headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      return (json.decode(response.body) as List)
          .map((stationJson) => mapper(stationJson))
          .toList();
    }
    print(response.body);
    return [];
  }

  @override
  Future<List<LogbookRestServiceModelRouteEntry>> fetchRouteEntries({
    required String groupId,
    required String vehicleId,
  }) async {
    final url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupId/vehicles/$vehicleId/routes');
    return (await callRestApi<LogbookRestServiceModelRouteEntry>(
      url: url,
      token: idToken!,
      mapper: (json) => LogbookRestServiceModelRouteEntry.fromMap(json),
    ));
  }

  Future<List<T>> callRestApi<T>({
    required Uri url,
    required String token,
    required T Function(dynamic) mapper,
  }) async {
    final response = await http.get(url, headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $token",
    });
    if (response.statusCode == 200) {
      return (json.decode(response.body) as List)
          .map((stationJson) => mapper(stationJson))
          .toList();
    }
    print(response.body);
    return [];
  }

  @override
  Future<int> sendStatsRequest({
    required String? stationId,
    required String startTs,
    required String endTs,
  }) async {
    if (endTs.isEmpty || startTs.isEmpty) return 400;
    var url = _statsBackendUrl() + "/specificStationPriceDevelopment";
    if (stationId == null || stationId.isEmpty) {
      stationId = "";
      url = _statsBackendUrl() + "/generalPriceDevelopment";
    }
    var response = await http.post(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: jsonEncode({
          "groupId": "$_groupId",
          "stationId": "$stationId",
          "beginTimestamp": "$startTs",
          "endTimestamp": "$endTs",
        }));
    print(response.statusCode);
    return response.statusCode;
  }

  @override
  Future<List<Stats>> fetchStats() async {
    var url = _statsBackendUrl() + "/groups/$_groupId/stats";
    late http.Response response;
    try {
      response = await http.get(Uri.parse(url), headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Access-Control-Allow-Origin": "*",
      });
    } catch (e) {
      print("Could not fetch stats $e");
      return [];
    }
    return (jsonDecode(response.body) as List<dynamic>)
        .map((it) => it as Map<String, dynamic>)
        .map((it) {
      var id = it["id"] as String;
      var price = (it["petrolPrice"] as List<dynamic>)
          .map((e) => StatsModelPrice.fromMap(e))
          .toList();
      return Stats(id: id, petrolPrice: price);
    }).toList();
  }

  @override
  void deleteStat({required String statId}) async {
    var url = _statsBackendUrl() + "/groups/$_groupId/stats/$statId";
    var response = await http.delete(Uri.parse(url), headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
    });
    print(response.statusCode);
  }

  @override
  Future<bool> login({required String email, required String password}) async {
    final url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$apiKey";
    late http.Response response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
          },
          body: jsonEncode({
            "email": email,
            "password": password,
            "returnSecureToken": true,
          }));
    } on Exception catch (ex) {
      print(ex.toString());
      return false;
    }
    print(response.statusCode);
    if (response.statusCode < 300 && response.statusCode >= 200) {
      idToken = jsonDecode(response.body)["idToken"];
      refreshToken = jsonDecode(response.body)["refreshToken"];
      _groupId = jsonDecode(response.body)["localId"];
      _isEnterprise = await fetchIsEnterprise(
          groupId: jsonDecode(response.body)["localId"]);
      expiresIn = DateTime.now().add(
          Duration(seconds: int.parse(jsonDecode(response.body)["expiresIn"])));
      print("Login correct, IdToken: $idToken");
      print("Set _groupId: $_groupId");
      print("Login correct, Set refreshToken: $refreshToken");
      print("Login correct, Set expiresIn: $expiresIn");
      return true;
    }
    return false;
  }

  @override
  Future<bool> register({
    required String name,
    required String email,
    required String password,
    required bool isEnterprise,
  }) async {
    final url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$apiKey";
    late http.Response response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
          },
          body: jsonEncode({
            "email": email,
            "password": password,
            "returnSecureToken": true,
          }));
    } on Exception catch (ex) {
      print(ex.toString());
      return false;
    }
    print(response.statusCode);
    if (response.statusCode < 300 && response.statusCode >= 200) {
      idToken = jsonDecode(response.body)["idToken"];
      refreshToken = jsonDecode(response.body)["refreshToken"];
      _groupId = jsonDecode(response.body)["localId"];
      expiresIn = DateTime.now().add(
          Duration(seconds: int.parse(jsonDecode(response.body)["expiresIn"])));
      print("Set IdToken: $idToken");
      print("Set _groupId: $_groupId");
      print("Set refreshToken: $refreshToken");
      print("Set expiresIn: $expiresIn");
      String? createdGroupID =
          await createGroup(name: name, isEnterprise: isEnterprise);
      if (createdGroupID != null) {
        _groupId = createdGroupID;
        _isEnterprise = await fetchIsEnterprise(
            groupId: jsonDecode(response.body)["localId"]);
        print("Created group and set id: $createdGroupID");
        return true;
      }
    }
    print("Could not register: ${response.body}");

    return false;
  }

  Future<String?> createGroup(
      {required String name, required bool isEnterprise}) async {
    var url = _flottenBackendUrl() + "/groups";
    late http.Response response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer $idToken",
          },
          body: jsonEncode({
            "name": name,
            "isEnterprise": isEnterprise,
          }));
    } catch (e) {
      print("error creating group: $e");
      return null;
    }
    return response.body;
  }

  @override
  Future<bool> logout() async {
    _groupId = null;
    idToken = null;
    refreshToken = null;
    expiresIn = null;
    return true;
  }

  Future<bool> refreshLogin() async {
    final url = "https://identitytoolkit.googleapis.com/v1/token?key=$apiKey";
    late http.Response response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
          },
          body: jsonEncode({
            "grant_type": "refresh_token",
            "refresh_token": refreshToken,
          }));
    } on Exception catch (ex) {
      print(ex.toString());
      return false;
    }
    if (response.statusCode < 300 && response.statusCode >= 200) {
      idToken = jsonDecode(response.body)["id_token"];
      refreshToken = jsonDecode(response.body)["refresh_token"];
      expiresIn = DateTime.now().add(Duration(
          seconds: int.parse(jsonDecode(response.body)["expires_in"])));
      print("Got new refresh token");
      return true;
    }
    print("Failed to get refresehd token: ${response.body}");
    return false;
  }

  @override
  Future<bool> createRouteEntry({
    required String selectedVehicleId,
    required String departure,
    required String destination,
    required String distanceMeters,
    required int consumptionMilliliter,
    required int totalCostsCent,
    required String groupID,
    required String name,
  }) async {
    final url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupID/vehicles/$selectedVehicleId/routes');
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer $idToken",
        },
        body: jsonEncode({
          "id": randomNumer(),
          "departure": departure,
          "destination": destination,
          "name": name,
          "distanceMeters": distanceMeters,
          "consumptionMilliliter": consumptionMilliliter,
          "totalCostsCent": totalCostsCent,
          "creationDateIsoString": DateTime.now().toIso8601String(),
        }));
    if (response.statusCode == 200) {
      print("Success creating new route entry");
      return true;
    }
    print("Failure creating new route entry: ${response.statusCode}");
    return false;
  }

  @override
  Future<bool> createRefuelEntry({
    required String selectedVehicleId,
    required String position,
    required String mileageMeters,
    required int consumptionMilliliter,
    required int pricePerLiter,
    required int totalCostsCent,
    required String groupID,
    required String name,
  }) async {
    final url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupID/vehicles/$selectedVehicleId/refuels');
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer $idToken",
        },
        body: jsonEncode({
          "id": randomNumer(),
          "position": position,
          "mileageMeters": mileageMeters,
          "consumptionMilliliter": consumptionMilliliter,
          "totalCostsCent": totalCostsCent,
          "pricePerLiterCents": pricePerLiter,
          "creationDateIsoString": DateTime.now().toIso8601String(),
        }));
    if (response.statusCode == 200) {
      print("Success creating new refuel entry");
      return true;
    }
    print("Failure creating new refuel entry: ${response.statusCode}");
    return false;
  }

  @override
  Future<List<GasStationModel>> fetchStations() async {
    final url = Uri.parse('${_backendUrl()}/stations');
    final http.Response response = await http.get(url, headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*"
    });
    return (json.decode(response.body) as List).map((stationJson) {
      return GasStationModel.fromMap(stationJson);
    }).toList();
  }

  @override
  String? get groupId => _groupId;

  @override
  Future<bool> leaveGroup({required String groupId}) async {
    final url = Uri.parse('${_flottenBackendUrl()}/groups/$groupId');
    var response = await http.delete(url, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      print("Success joining group");
      return true;
    }
    print("Failure joining group: ${response.statusCode}");
    return false;
  }

  @override
  Future<bool> joinGroup({required String currentGroupId}) async {
    final url = Uri.parse('${_flottenBackendUrl()}/groups/$currentGroupId');
    var response = await http.post(url, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      print("Success joining group");
      return true;
    }
    print("Failure joining group: ${response.statusCode}");
    return false;
  }

  @override
  Future<bool> deleteVehicle({
    String? groupID,
    String? vehicleID,
  }) async {
    final url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupID/vehicles/$vehicleID');
    var response = await http.delete(url, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      print("Success deleting vehicle");
      return true;
    }
    print("Failure deleting vehicle: ${response.statusCode}");
    return false;
  }

  @override
  Future<bool> deleteRefuelEntry({
    required String entryId,
    required String vehicleId,
    required String groupId,
  }) async {
    final url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupId/vehicles/$vehicleId/refuels/$entryId');
    var response = await http.delete(url, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      print("Success deleting refuel entry");
      return true;
    }
    print("Failure deleting refuel entry: ${response.statusCode}");
    return false;
  }

  @override
  Future<bool> deleteRouteEntry({
    required String entryId,
    required String vehicleId,
    required String groupId,
  }) async {
    final url = Uri.parse(
        '${_flottenBackendUrl()}/groups/$groupId/vehicles/$vehicleId/routes/$entryId');
    var response = await http.delete(url, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer $idToken",
    });
    if (response.statusCode == 200) {
      print("Success deleting route entry");
      return true;
    }
    print("Failure deleting route entry: ${response.statusCode}");
    return false;
  }

  @override
  Future<String> fetchPrices({
    required List<GasStationModel> stations,
  }) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final http.Response response = await http.put(
        Uri.parse('${_backendUrl()}/prices'),
        body: json.encode(Map.fromIterable(stations,
            key: (s) => s.id,
            value: (s) =>
                s.gasEntries.isEmpty ? null : s.gasEntries.first.timestamp)),
        headers: headers);
    return response.body;
  }

  @override
  bool isLoggedId() {
    return _groupId != null;
  }

  @override
  bool isLoggedIn1() {
    return _groupId != null;
  }

  Future<String?> fetchIsEnterprise({required String groupId}) async {
    final url = Uri.parse('${_flottenBackendUrl()}/groups/$groupId/enterprise');
    final http.Response response = await http.get(url, headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*"
    });
    var body = response.body;
    if (body.isEmpty) {
      _isEnterprise == null;
    } else {
      _isEnterprise = body;
    }
    if (response.statusCode == 200 && _isEnterprise != null) {
      final url2 = Uri.parse('${_flottenBackendUrl()}/alive');
      final http.Response response2 = await http.get(url2, headers: {
        "Accept": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      if (response2.statusCode == 200) {
        return _isEnterprise;
      }
    }
    return null;
  }

  @override
  String? get isEnterprise {
    var enterprise = _isEnterprise;
    if (enterprise == null || enterprise.isEmpty) {
      return null;
    }
    return enterprise;
  }

  @override
  bool get isLoggenInLogbook => _groupId != null;
}
