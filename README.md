# spritradar

A new Flutter project.

## Getting Started

run `flutter pub get` to get the dependencies

run `flutter devices` to list all connected devices

run `flutter run -d <deviceID>` to run it on a specific device

run `flutter run -d chrome` to run it in chrome

run `flutter run -d chrome --release` to run it in chrome in release mode